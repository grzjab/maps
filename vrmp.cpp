/** @file vrmp.cpp
 *  @brief Main file for computation of persistence of a map using Vietoris Rips simplicial complex
 *  Contains an implementation of an algorithm from the work Edelsbrunner, H., Jabłonski, G., & Mrozek, M. The Persistent Homology of a Self-map.
 *  @author Grzegorz Jablonski
 *  @year 2012-2014
 *  @bug No known bugs.
 */
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <algorithm>
#include <string>
#include <sstream>
#include <cstdlib>
#include <boost/foreach.hpp>

#include "rips.hpp"
#include "capd/rings/Zp.h"
#include "PersistenceDgm.hpp"
#include "utils.hpp"
#include "filtration.hpp"
#include "map.hpp"
#include "Interval.h"
#include "Reduction.hpp"
#include "utils.h"
#include "PointTypes.hpp"
#include "Types.hpp"
#include "Towers.h"
#include "MapMatrix.h"
#include "Options.h"

#include "capd/auxil/Stopwatch.h"

using namespace std;
using namespace mappers;

// field Z_p parameter
int Zp::p = 1009;

//**********CONSTANTS*******************
const int NOTFOUND = -1;
const int NONE = -1;
const double INF = 1000000;

/**
 * @brief application of eigenfunctor E_{lambda} or generalized eigenfunctor E_{lambda,2}
 *
 * @param phiSequence matrix representations of \varphi_1,..,\varphi_n
 * @param psiSequence matrix representations of \psi_1,..,\psi_n
 * @param epsilonSequence matrix representations of v_1,..,v_n (inclusion induced maps
 * @param lambda candidate for eigenvalue
 * @param eigenBase basis of the tower computed by the matching algorithm
 * @param prsDgm lambda-persistence diagram
 * @param 
 */
void applyFunctor(
		TowerType   &phiSequence,
		TowerType   &psiSequence, 
		TowerType   &epsilonSequence,
		ScalarType lambda,	
		SpaceTowerType &eigenBase,
		PersistenceDgm<MatrixType,MatrixType,IntVectorType>& prsDgm,
		bool useGeneralizedEigenvector
		)
{
	Stopwatch swTot;

	if(!useGeneralizedEigenvector){
		eigenBase = applyEigenfunctor(phiSequence,psiSequence,lambda);
	}else{
		eigenBase = applyGeneralizedEigenfunctor(phiSequence,psiSequence,lambda);
	}


	TowerType restrictedKsiTower;

	for(int k=0;k<eigenBase.size()-1;k++)
	{
		MatrixType rksi;

		//compute restriction of \ksi on new bases (quotient groups)
		if(epsilonSequence[k].numberOfColumns()>0){
			matrixRestriction(epsilonSequence[k],eigenBase[k],eigenBase[k+1],rksi);
		}
		restrictedKsiTower.push_back(rksi);
	}

	std::cout<<"Step 3 time: "<<swTot<<"\n";
	swTot.reset();
	prsDgm = PersistenceDgm<MatrixType,MatrixType,IntVectorType>(restrictedKsiTower);
	std::cout<<"Step 4 time: "<<swTot<<"\n";
}

int main(int argc, char *argv[] )
{

	Options opt(argc,argv);	
	if(!opt.ok) return 0;

	ofstream outFile(opt.outputFile.c_str());
	int maxDim = opt.maxDim;
	int minDim = opt.minDim;

	ifstream lambdaFile(opt.lambdaFile.c_str());
	//read candidates for eigenvalues
	vector<ScalarType> lambda = readLambda<ScalarType>(lambdaFile);

	//read point cloud data S and an approximation g of a map f
	ifstream pointsFile(opt.pointsFile.c_str());
	vector<pair<rips::euclideanPoint, int> > mapG = prepareMap(pointsFile);
	vector<rips::euclideanPoint> points = domainPoints(mapG); 
	vector<int> gMapVec = qProj(mapG);

	//compute them max distance between points in S, necessary in construction of VR filtration
	typedef vector<rips::euclideanPoint>::iterator point_iterator;
	double max_dist = 0;
	vector<double> distances;
	for(point_iterator it=points.begin();it!=points.end();it++)
		for(point_iterator it2=it;it2!=points.end();it2++)
		{
			max_dist=std::max(rips::computeDistance( *it, *it2 ),max_dist);

			distances.push_back(rips::computeDistance( *it, *it2 ));
		}
	sort(distances.begin(), distances.end());

	Stopwatch swTot;

	//build filtration based on the Vietoris Rips complex on $S$
	PRINT("building VR complex of domain... eps:"<<distances[(distances.size()-1)*opt.maximumEpsilon])
		rips::simplicialComplex<rips::euclideanPoint, ScalarType> 
		filtration( points ,  distances[(distances.size()-1)*opt.maximumEpsilon], maxDim + 1);
	//prepare two maps: \kappa' and \imath induced by the map g and the inclusion map
	Map<ScalarType> g(gMapVec);
	Map<ScalarType> inclusion;
	g.setDomRng(&filtration,&filtration);
	//build filtration \bar{\mathcal{K}} of domains	
	rips::simplicialComplex<rips::euclideanPoint, ScalarType> domainFiltration(filtration,g);

	//let the simplicial maps know what is the domain and the range
	g.setDomRng(&domainFiltration,&filtration);
	inclusion.setDomRng(&domainFiltration,&filtration);

	//compute persistence diagrams of filtrations using reduction algorithms
	GradedSparseMatrixType reducedBoundary,subsetReducedBoudnary;
	GradedDiagramType diagrams,domDiagrams;
	vector<vector<int> > domLow,low;
	std::cout<<"Rips complexes building time: "<<swTot<<"\n";

	swTot.reset();
	std::cout<<"Reduction...\n";
	reduceTwist(reducedBoundary, diagrams, low, filtration);
	std::cout<<"...\n";
	reduceTwist(subsetReducedBoudnary, domDiagrams, domLow, domainFiltration);

	std::cout<<"Reduction finished\nPreparing map matrices\n";

	std::cout<<"Reduction time: (step 1)"<<swTot<<"\n";

	swTot.reset();
	GradedSparseMatrixType psiMatrix,phiMatrix;
	//following two lines are auxiliary function that reduces computation time by storing information in fMatrix and insertionMatrix about maps in the homology
	prepareMapMatrix(g,domDiagrams,diagrams,subsetReducedBoudnary,reducedBoundary,low,maxDim,domainFiltration,filtration,psiMatrix);
	prepareMapMatrix(inclusion,domDiagrams,diagrams,subsetReducedBoudnary,reducedBoundary,low,maxDim,domainFiltration,filtration,phiMatrix);

	std::cout<<"Map matrices building time: (step 2)"<<swTot<<"\n";

	//reset timer
	swTot.reset();
	//Prepare critical values of the Vietoris Rips complex
	vector<double> criticalValues = prepareCriticalValues(domDiagrams, diagrams, opt.matrices, minDim, maxDim);

	//create tower in Pairs(Vect) with representation described in the thesis
	GradedTowerType psiSequence, phiSequence, epsilonSequence;
	makeMapTower(criticalValues, psiMatrix, domDiagrams,diagrams, maxDim, psiSequence);
	makeMapTower(criticalValues, phiMatrix, domDiagrams,diagrams, maxDim, phiSequence);
	makeKsiTower(criticalValues, domDiagrams, maxDim, epsilonSequence);

	std::cout<<"Critical values: " << criticalValues.size()<<"\n";	
	for(int i=minDim;i<=maxDim;i++)
	{
		std::cout<<"Dimension="<<i<<"\n";
		outFile << i <<"\n";

		BOOST_FOREACH(ScalarType eigenvalue, lambda)
		{
			std::cout<<"Eigenvalue="<<eigenvalue<<"\n";
			outFile << eigenvalue << "\n";

			vector<QuotientSpaceType > eigenBase; 
			PersistenceDgm<MatrixType,MatrixType,IntVectorType> prsDgm;

			//applying E_\lambda eigenfunctor in dimension i for an eigenvalue candidate stored in variable "lambda"
			applyFunctor(phiSequence[i],psiSequence[i],epsilonSequence[i],eigenvalue,eigenBase,prsDgm,opt.generalizedEigenvectors);
			typedef PersistenceDgm<MatrixType,MatrixType,IntVector>::HomologyClass HC;
			BOOST_FOREACH(HC hc, prsDgm.homologyClasses ) 
			{
				std::cout<<"["<<criticalValues[hc.birth]<<","<<criticalValues[hc.death]<<")"<<std::endl;
				outFile << criticalValues[hc.birth] << " "<< criticalValues[hc.death] << "\n";
				VectorType cr(eigenBase[hc.birth].first*prsDgm.V[hc.birth].column(hc.column));
			}


		}
		std::cout<<"######\n";
		outFile <<"#####\n";
	}

	std::cout<<"Total time: "<<swTot<<"\n";

}
