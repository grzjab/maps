#if !defined(_DEBUG_H_)
#define _DEBUG_H_

#define DEBUG(x) do { std::cerr << x; } while (0);
#define PRINT(x) do { std::cout << x; } while (0);
#define SHOW(x) do { std::cout << x; } while (0); std::cout<<"\r"; std::cout.flush();

#endif //_DEBUG_H_
