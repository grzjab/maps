/** @file Towers.h
 *  @brief
 *  This file contains definitions of functions to work with towers of pairs of vectors spaces
 *  @author Grzegorz Jablonski
 *
 *  @year 2014
 *  @bug No known bugs.
 */
#if !defined(_TOWERS_H_)
#define _TOWERS_H_
#include <vector>

#include "Types.hpp"

#include "Interval.h"

namespace mappers{
	/**
	 * @brief Build graded tower of linear maps that are inclusions between linear spaces build from dom at
	 * parameters given in epsilons. 
	 *
	 * @param epsilons sequence of real values between which one builds the linear maps
	 * @param dom persistence diagrams in consequting levels
	 * @param dim 
	 * @param ksi sequence of linear maps in every level
	 */
	void makeKsiTower(const std::vector<double> &epsilons,const GradedDiagramType &dom, int dim, GradedTowerType &ksi);
	/**
	 * @brief 
	 *
	 * @param epsilons
	 * @param fMatrix
	 * @param dom
	 * @param rng
	 * @param dim
	 * @param tower
	 */
	void makeMapTower(const std::vector<double> &epsilons,const GradedSparseMatrixType &fMatrix,const GradedDiagramType &dom,
			const GradedDiagramType &rng, int dim, GradedTowerType &tower );

	/**
	 * @brief Compute tower in Vect after applygin the eigenfunctor E_{lambda} to the tower in Pairs(Vect) 
	 * represented by matrices in phiSequence and psiSequence
	 *
	 * @param phiSequence sequence of linear maps
	 * @param psiSequence sequence of linear maps
	 * @param lambda candidate for the eigenvalue
	 *
	 * @return tower in Vect
	 */
	SpaceTowerType applyEigenfunctor(const TowerType   &phiSequence, const TowerType   &psiSequence, 
			ScalarType lambda); 
	/**
	 * @brief Compute tower in Vect after applygin the generalized eigenfunctor E_{lambda,2} to the tower in Pairs(Vect) 
	 * represented by matrices in phiSequence and psiSequence
	 *
	 * @param phiSequence sequence of linear maps
	 * @param psiSequence sequence of linear maps
	 * @param lambda candidate for the eigenvalue
	 *
	 * @return tower in Vect
	 */
	SpaceTowerType applyGeneralizedEigenfunctor(const TowerType   &phiSequence, const TowerType   &psiSequence, 
			ScalarType lambda); 
}
#endif //_TOWERS_H_
