/** @file rips.hpp
 *  @brief Implementation of Vietoris Rips filtration
 *  @author Pawel Dlotko, Huber Wagner, Grzegorz Jabloński
 *  @year 2014
 *  @bug No known bugs.
 */
#if !defined(_RIPS_HPP_)
#define _RIPS_HPP_
#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <algorithm>
#include <cstdio>
#include <iterator>
#include <ctime>
#include <sstream>
#include <fstream>
#include <limits>
#include <stdint.h>

#include <boost/foreach.hpp>

#include "filtration.hpp"
#include "map.hpp"
#include "PointTypes.hpp"
#include "Debug.h"


extern const int NOTFOUND;
extern const int NONE;

using namespace mappers;

namespace rips
{
//Class of simplex and simplicial (Rips) complex
template <typename T,typename ScalarTypeT> class simplicialComplex;

/**
 * @brief Implementation of simplex
 *
 * @tparam T coordinate of vertex
 * @tparam ScalarTypeT coefficients in the boundary
 */
template <typename T, typename ScalarTypeT>
class simplex
{
public:
    simplex()
    {
        this->nr = -1;
    };
    int dim()
		{
			if(vertices.size()==0) return 0;
			return vertices.size()-1;
		}
    friend class simplicialComplex<T,ScalarTypeT>;

    friend std::ostream& operator << ( std::ostream& out , simplex* sim )
    {
        //redHom standard:
        if ( sim->dim() == 0 )
        {
            out << sim->nr;
        }
        else
        {
            for ( size_t i = 0 ; i != sim->vertices.size() ; ++i )
            {
                out << sim->vertices[i]->nr << " ";
            }
        }
        return out;
    }

//private:
    std::vector< T > vertCoords;
    std::vector< simplex<T,ScalarTypeT>* > vertices;
    double gradationLevel;
    std::vector< simplex<T,ScalarTypeT>* > boundary;
    std::vector< simplex<T,ScalarTypeT>* > coBoundary;
    int nr;
		std::vector<int> sortedVertices()
		{
			std::vector<int> res;
			for(typename     std::vector< simplex<T,ScalarTypeT>* >::iterator it=vertices.begin();it!=vertices.end();it++)
				res.push_back((*it)->nr);
			if( nr!=-1) res.push_back(nr);
			sort(res.begin(),res.end());
			return res;
		}
};

template< typename T, typename ScalarTypeT>
bool compare(simplex<T,ScalarTypeT>* first, simplex<T,ScalarTypeT>* second )
{
    return (first->gradationLevel < second->gradationLevel);
}


/**
 * @brief Implementation of an abstract simplicial complex
 *
 * @tparam T coordinate of vertex
 * @tparam ScalarTypeT field coefficients 
 */
template <typename T, typename ScalarTypeT>
class simplicialComplex:public Filtration<ScalarTypeT>
{
public:
	  std::vector< std::vector< double > > gradationOfSimplicesInFollowingDimensions;
		std::vector<std::map<std::vector<int>, int> > vert2simp;

    simplicialComplex(){};
    simplicialComplex( const std::vector< T >& pointCloud , double epsilon , int maxCreatedDimension );
    simplicialComplex( simplicialComplex<T,ScalarTypeT> &source, Map<ScalarTypeT> f );

    friend class simplex<T,ScalarTypeT>;

    std::vector<std::vector<std::vector<int> > > createBoundaryMatrices();
	std::vector<std::vector<std::vector<std::pair<int, ScalarTypeT> > > > sortedBoundary();

    void outputMaxSimplicesToFile( char* filename );

    friend std::ostream& operator << ( std::ostream& out , simplicialComplex& cmplx )
    {
        for ( size_t i = 0 ; i != cmplx.elements.size() ; ++i )
        {
            out << "Dimension : " << i << "\n";
            for ( size_t j = 0 ; j != cmplx.elements[i].size() ; ++j )
            {
                out << cmplx.elements[i][j] << "\n";
                out << "Gradation : " << cmplx.elements[i][j]->gradationLevel << "\n";
                out << "Boundary elements : \n";
                for ( size_t k = 0 ; k != cmplx.elements[i][j]->boundary.size() ; ++k )
                {
                    out << cmplx.elements[i][j]->boundary[k] << " , ";
                }
                out << "\n coboundary elements : \n";
                for ( size_t k = 0 ; k != cmplx.elements[i][j]->coBoundary.size() ; ++k )
                {
                    out << cmplx.elements[i][j]->coBoundary[k] << " , ";
                }
                out << "\n\n\n";
            }
        }
        return out;
    }

    ~simplicialComplex()
    {
        for ( size_t i = 0 ; i != this->elements.size() ; ++i )
        {
            for ( size_t j = 0 ; j != this->elements[i].size() ; ++j )
            {
                delete this->elements[i][j];
            }
        }
    }
    std::vector< simplex<T,ScalarTypeT>* > createVertices( const std::vector< T >& pointCloud );
    std::vector< simplex<T,ScalarTypeT>* > createEdges( std::vector< simplex<T,ScalarTypeT>* >& vertices , std::map< simplex<T,ScalarTypeT>* , std::set<simplex<T,ScalarTypeT>*> >& neigh , double epsilon );
    std::map< simplex<T,ScalarTypeT>* , std::set< simplex<T,ScalarTypeT>* > > createSimplicesNeigh( std::vector< simplex<T,ScalarTypeT>* >& edges ,  std::map< simplex<T,ScalarTypeT>* , std::set<simplex<T,ScalarTypeT>*> >& neigh);
    void creadeHigherDimensionalSimplices( std::vector< simplex<T,ScalarTypeT>* >& edges  , std::map< simplex<T,ScalarTypeT>* , std::set<simplex<T,ScalarTypeT>*> >& neigh , double epsilon , int maxCreatedDimension );
	double simplexGradation(int dim,int index);
	int vertices2simplex(vector<int> vertices);	
	int dimension() { return elements.size()-1; };

	void createSimplicialComplex( const std::vector< T >& pointCloud );

    std::vector< std::vector< simplex<T,ScalarTypeT>* > > elements;
    double epsilon;
    int maxCreatedDimension;
};

template <typename T, typename ScalarTypeT>
double simplicialComplex<T,ScalarTypeT>::simplexGradation(int dim,int index)
{
	return gradationOfSimplicesInFollowingDimensions[dim][index];
}

template <typename T, typename ScalarTypeT>
int simplicialComplex<T,ScalarTypeT>::vertices2simplex(vector<int> vertices)
{ 
	int dim = vertices.size()-1;
	typename std::map<std::vector<int>, int>::iterator it = vert2simp[dim].find(vertices);
	if(it ==  vert2simp[dim].end())
		return NOTFOUND;
	return it->second;
}

template <typename T, typename ScalarTypeT>
void simplicialComplex<T,ScalarTypeT>::outputMaxSimplicesToFile( char* filename )
{
    std::ofstream file;
    file.open( filename );
    for ( size_t dim = 0 ; dim != this->elements.size() ; ++dim )
    {
        for ( size_t i = 0 ; i != this->elements[dim].size() ; ++i )
        {
            //if coboundary of this->elements[dim][i] is empty, then output the simplex to the file.
            if ( this->elements[dim][i]->coBoundary.size() == 0 )
            {
                //write this simplex to the file.
                file << this->elements[dim][i] << std::endl;
            }
        }
    }
    file.close();
}

//compute the sign of face in the boundary in simplex
template <typename ScalarTypeT>
ScalarTypeT sign(std::vector<int> &simplex, std::vector<int> &face)
{
	if(simplex.size()==2)
	{
		if(face[0]==simplex[0]) return ScalarTypeT(-1);
		return ScalarTypeT(1);
	}
	else if(simplex.size()==3)
	{
		if(face[0]==simplex[1]) return ScalarTypeT(1);
		if(face[1]==simplex[2]) return ScalarTypeT(-1);
		return ScalarTypeT(1);
	}
}

	template <typename T, typename ScalarTypeT>
std::vector<std::vector<std::vector<std::pair<int, ScalarTypeT> > > > simplicialComplex<T,ScalarTypeT>::sortedBoundary()
{
	std::vector<std::vector<std::vector<std::pair<int,ScalarTypeT> > > > result;

	//partial_0!!
	std::vector< std::vector<std::pair<int,ScalarTypeT> > > partial0(this->elements[0].size());
	result.push_back(partial0);


	for ( size_t i = 1 ; i != this->elements.size() ; ++i )
	{
		//first create a map simplex* -> its position for dimension i-1
		std::map< simplex<T,ScalarTypeT>* , int > simplex2pos;
		for ( size_t j = 0 ; j != this->elements[i-1].size() ; ++j )
		{
			simplex2pos.insert( std::make_pair( this->elements[i-1][j] , j ) );
		}
		std::vector< std::vector<std::pair<int,ScalarTypeT> > > bdMatrix;
		for ( size_t j = 0 ; j != this->elements[i].size() ; ++j )
		{
			std::vector<std::pair<int,ScalarTypeT> > bdOfThisElement;
			vector<int> verticesOfThisElement = this->elements[i][j]->sortedVertices();
			for ( size_t k = 0 ; k != this->elements[i][j]->boundary.size() ; ++k )
			{
				vector<int> verticesOfBoundary = this->elements[i][j]->boundary[k]->sortedVertices();
				bdOfThisElement.push_back(std::pair<int,ScalarTypeT>(simplex2pos.find(this->elements[i][j]->boundary[k])->second,sign<ScalarTypeT>(verticesOfThisElement,verticesOfBoundary)));
			}
			std::sort( bdOfThisElement.begin() , bdOfThisElement.end() );
			bdMatrix.push_back( bdOfThisElement );
		}
		result.push_back( bdMatrix );
	}
	return result;

}


template <typename T, typename ScalarTypeT>
std::vector<std::vector<std::vector<int> > > simplicialComplex<T,ScalarTypeT>::createBoundaryMatrices()
{
    std::vector<std::vector<std::vector<int> > > result;



    //partial_0!!
    std::vector< std::vector<int> > partial0;
    for ( size_t i = 0 ; i != this->elements[0].size() ; ++i )
    {
        std::vector<int> s;
        partial0.push_back(s);
    }
    result.push_back(partial0);


    for ( size_t i = 1 ; i != this->elements.size() ; ++i )
    {
        //first create a map simplex* -> its position for dimension i-1
        std::map< simplex<T,ScalarTypeT>* , int > simplex2pos;
        for ( size_t j = 0 ; j != this->elements[i-1].size() ; ++j )
        {
            simplex2pos.insert( std::make_pair( this->elements[i-1][j] , j ) );
        }
        std::vector< std::vector<int> > bdMatrix;
        for ( size_t j = 0 ; j != this->elements[i].size() ; ++j )
        {
            std::vector<int> bdOfThisElement;
            for ( size_t k = 0 ; k != this->elements[i][j]->boundary.size() ; ++k )
            {
                bdOfThisElement.push_back(simplex2pos.find(this->elements[i][j]->boundary[k])->second);
            }
            std::sort( bdOfThisElement.begin() , bdOfThisElement.end() );
            bdMatrix.push_back( bdOfThisElement );
        }
        result.push_back( bdMatrix );
    }
    return result;
}//createBoundaryMatrices


template <typename T, typename ScalarTypeT>
std::vector< simplex<T,ScalarTypeT>* > simplicialComplex<T,ScalarTypeT>::createVertices( const std::vector< T >& pointCloud )
{
    //create vertices
    PRINT("adding vertices\n")
    std::vector< simplex<T,ScalarTypeT>* > vertices;
    for ( size_t i = 0 ; i != pointCloud.size() ; ++i )
    {
        simplex<T,ScalarTypeT>* vertex = new simplex<T,ScalarTypeT>;
        vertex->nr = i;
        //SHOW("adding vertex nr:" << i)
        vertex->vertCoords.push_back( pointCloud[i] );
        vertex->gradationLevel = 0;
        vertices.push_back( vertex );
    }
    this->elements.push_back( vertices );
    return vertices;
}//createVertices




template <typename T, typename ScalarTypeT>
std::vector< simplex<T,ScalarTypeT>* > simplicialComplex<T,ScalarTypeT>::createEdges( std::vector< simplex<T,ScalarTypeT>* >& vertices , std::map< simplex<T,ScalarTypeT>* , std::set<simplex<T,ScalarTypeT>*> >& neigh , double epsilon )
{
	PRINT("Preparing neigbour list... ")
    //to create complex & avoid repetiotions we will use neighbourList
    std::vector< std::vector<int> > neighbourList;
    std::map< std::pair< simplex<T,ScalarTypeT>* , simplex<T,ScalarTypeT>* > , double > distanceMap;
    for ( size_t i = 0 ; i != vertices.size() ; ++i )
    {
        std::vector<int> neighOfIth;
        for ( size_t j = i+1 ; j != vertices.size() ; ++j )
        {
             double dist = computeDistance( vertices[i]->vertCoords[0] , vertices[j]->vertCoords[0] );
             if ( dist <= epsilon )
             {
                   neighOfIth.push_back( j );
                   neigh.find( vertices[i] )->second.insert( vertices[j] );
                   distanceMap.insert( std::make_pair( std::make_pair(vertices[i],vertices[j]) , dist) );
                   distanceMap.insert( std::make_pair( std::make_pair(vertices[j],vertices[i]) , dist) );
             }
        }
        neighbourList.push_back( neighOfIth );
    }
    PRINT("done!\n")

    int dim = 1;

    //creating edges:
    PRINT("creating edges...\n")
    std::vector< simplex<T,ScalarTypeT>* > edges;
    for ( size_t i = 0 ; i != this->elements[ dim-1 ].size() ; ++i )
    {
        for ( size_t j = 0 ; j != neighbourList[i].size() ; ++j )
        {
            //create an edge from vertices[i] to vertices[j] having gradation distanceList[i][j]
            simplex<T,ScalarTypeT>* edge = new simplex<T,ScalarTypeT>;
            edge->vertCoords.push_back( vertices[i]->vertCoords[0] );
            edge->vertCoords.push_back( vertices[neighbourList[i][j]]->vertCoords[0] );
            edge->gradationLevel = distanceMap.find( std::make_pair( vertices[i] , vertices[ neighbourList[i][j] ] ) )->second;
            edge->boundary.push_back( vertices[i] );
            edge->boundary.push_back( vertices[ neighbourList[i][j] ] );
            edge->vertices.push_back( vertices[i] );
            edge->vertices.push_back( vertices[ neighbourList[i][j] ] );
            vertices[i]->coBoundary.push_back( edge );
            vertices[ neighbourList[i][j] ]->coBoundary.push_back( edge );
            edges.push_back(edge);
            //SHOW("processing vertex: "<<i << "neigbour: "<< j)
        }
    }
    PRINT("done!\n")
    this->elements.push_back(edges);
    return edges;
}

template <typename T, typename ScalarTypeT>
std::map< simplex<T,ScalarTypeT>* , std::set< simplex<T,ScalarTypeT>* > > simplicialComplex<T,ScalarTypeT>::createSimplicesNeigh( std::vector< simplex<T,ScalarTypeT>* >& edges  , std::map< simplex<T,ScalarTypeT>* , std::set<simplex<T,ScalarTypeT>*> >& neigh)
{

    std::map< simplex<T,ScalarTypeT>* , std::set< simplex<T,ScalarTypeT>* > > simplicesNeigh;
    for ( size_t i = 0 ; i != edges.size() ; ++i )
    {
        std::set< simplex<T,ScalarTypeT>* > s;
        simplicesNeigh.insert( std::make_pair( edges[i] , s ) );
        //pick an edge and compute its neighbours
        for ( typename std::set<simplex<T,ScalarTypeT>*>::iterator it = neigh.find(edges[i]->boundary[0])->second.begin() ; it != neigh.find(edges[i]->boundary[0])->second.end() ; ++it )
        {
             if ( neigh.find(edges[i]->boundary[1])->second.find( *it ) != neigh.find(edges[i]->boundary[1])->second.end() )
             {
                 simplicesNeigh.find( edges[i] )->second.insert( *it );
             }
        }
    }

    return simplicesNeigh;
}//createSimplicesNeigh


template <typename T, typename ScalarTypeT>
void simplicialComplex<T,ScalarTypeT>::creadeHigherDimensionalSimplices( std::vector< simplex<T,ScalarTypeT>* >& edges  , std::map< simplex<T,ScalarTypeT>* , std::set<simplex<T,ScalarTypeT>*> >& neigh , double epsilon , int maxCreatedDimension )
{
    std::map< simplex<T,ScalarTypeT>* , std::set< simplex<T,ScalarTypeT>* > > simplicesNeigh = this->createSimplicesNeigh( edges  , neigh );

    int dim = 1;
    for ( dim = 2 ; dim <= maxCreatedDimension ; ++dim )
    {
        std::map< simplex<T,ScalarTypeT>* , std::set< simplex<T,ScalarTypeT>* > > simplicesNeighNext;

        std::vector< simplex<T,ScalarTypeT>* > elems;
        for ( size_t i = 0 ; i != this->elements[ dim-1 ].size() ; ++i )
        {
             //if there is nothing to create, continue.
             if ( simplicesNeigh.find( this->elements[ dim-1 ][i] )->second.size() == 0 ){continue;}

             for ( typename std::set< simplex<T,ScalarTypeT>* >::iterator it = simplicesNeigh.find(this->elements[ dim-1 ][i])->second.begin() ; it != simplicesNeigh.find( this->elements[ dim-1 ][i] )->second.end() ; ++it )
             {
                 simplex<T,ScalarTypeT>* elem = new simplex<T,ScalarTypeT>;

                 //fill in vertices:
                 for ( size_t ver = 0 ; ver != this->elements[ dim-1 ][i]->vertices.size() ; ++ver )
                 {
                     elem->vertices.push_back( this->elements[ dim-1 ][i]->vertices[ver] );
                 }
                 elem->vertices.push_back( *it );

                 //fill simplicesNeighNext map:
                 std::set< simplex<T,ScalarTypeT>* > neighsOfNewSimplex;
                 for ( typename std::set< simplex<T,ScalarTypeT>* >::iterator nb = simplicesNeigh.find(this->elements[ dim-1 ][i])->second.begin() ; nb != simplicesNeigh.find( this->elements[ dim-1 ][i] )->second.end() ; ++nb )
                 {
                     if ( neigh.find( *it )->second.find( *nb ) != neigh.find( *it )->second.end())
                     {
                          neighsOfNewSimplex.insert( *nb );
                     }
                 }
                 simplicesNeighNext.insert( std::make_pair( elem , neighsOfNewSimplex ) );

                 //set vertCoords for elem:
                 for ( size_t j = 0 ; j != this->elements[ dim-1 ][i]->vertCoords.size() ; ++j )
                 {
                     elem->vertCoords.push_back( this->elements[ dim-1 ][i]->vertCoords[j] );
                 }
                 elem->vertCoords.push_back( (*it)->vertCoords[0] );



                 //set gradationLevel for elem:
                 double gradLev = this->elements[ dim-1 ][i]->gradationLevel;
                 for ( size_t j = 0 ; j != this->elements[ dim-1 ][i]->vertCoords.size() ; ++j )
                 {
                      double dst = computeDistance( this->elements[ dim-1 ][i]->vertCoords[j] , (*it)->vertCoords[0] );
                      if ( gradLev < dst ){gradLev = dst;}
                 }
                 elem->gradationLevel = gradLev;

                 //set boundary and coBoundary for elem:
                 //the easiest part is to set it for this->elements[ dim-1 ][i]:
                 this->elements[ dim-1 ][i]->coBoundary.push_back( elem );
                 elem->boundary.push_back( this->elements[ dim-1 ][i] );
                 //now we need to set all remaining elements:
                 for ( typename std::vector< simplex<T,ScalarTypeT>* >::iterator bd = this->elements[ dim-1 ][i]->boundary.begin() ; bd != this->elements[ dim-1 ][i]->boundary.end() ; ++bd )
                 {
                     for ( typename std::vector< simplex<T,ScalarTypeT>* >::iterator cbd = (*bd)->coBoundary.begin() ; cbd != (*bd)->coBoundary.end() ; ++cbd )
                     {
                          if ( *cbd == elem ){continue;}
                          //if (*cbd) has (*it) as one of vertices, then we are done!
                          for ( typename std::vector<T>::iterator fi = (*cbd)->vertCoords.begin(); fi != (*cbd)->vertCoords.end() ; ++fi )
                          {
                              if ( (*it)->vertCoords[0] == *fi )
                              {
                                  //set boundary and coboundary!
                                  (*cbd)->coBoundary.push_back( elem );
                                  elem->boundary.push_back( *cbd );
                              }
                          }
                     }
                 }
                 elems.push_back( elem );
             }
        }
        this->elements.push_back( elems );
        simplicesNeigh.clear();
        simplicesNeigh = simplicesNeighNext;
    }
}

template <typename T, typename ScalarTypeT>
/**
 * @brief Implementation of the algorithm vietorisRipsFiltration to construct Vietoris Rips filtration
 *
 * @param pointCloud Approximation of the space given by points in Euclidean space
 */
void simplicialComplex<T,ScalarTypeT>::createSimplicialComplex( const std::vector< T >& pointCloud )
{

    std::vector< simplex<T,ScalarTypeT>* > vertices = this->createVertices( pointCloud );


    std::map< simplex<T,ScalarTypeT>* , std::set<simplex<T,ScalarTypeT>*> > neigh;
    for ( size_t i = 0 ; i != vertices.size() ; ++i )
    {
        std::set<simplex<T,ScalarTypeT>*> s;
        neigh.insert( std::make_pair( vertices[i] , s ) );
    }

    std::vector< simplex<T,ScalarTypeT>* > edges = createEdges( vertices , neigh , epsilon );


		if ( maxCreatedDimension > 1)
		{
			//creating higher dim elements
			creadeHigherDimensionalSimplices( edges  ,  neigh , epsilon , maxCreatedDimension );
		}

    //at the end it would be good to sort simplices in elements according to gradation, so after we can easilly produce boundary matrices.
    for ( size_t i = 0 ; i != this->elements.size() ; ++i )
    {
        std::sort( this->elements[i].begin() , this->elements[i].end() , compare<T,ScalarTypeT> );
    }

	Filtration<ScalarTypeT>::simp2vert.resize(dimension()+1);
	vert2simp.resize(dimension()+1);


    //two aims for this set of instructions: first is to fill in gradationOfSimplicesInFollowingDimensions vector, so at the end we will have length of barcodes.
    //second is to remove from all simplices vertCoords vector (although complex can not be written anymore after that, since one will get seg fault) -- therefore this
    //instruction is commented for now!
    //This is to spare the memory, since we need vertCoords for construction, but we do not need it after.
    //The same story with coboundary list, although coBoundary maybe also delted on fly when needed.
    for ( size_t i = 0 ; i != this->elements.size() ; ++i )
    {
        std::vector< double > grad;
        for ( size_t j = 0 ; j != this->elements[i].size() ; ++j )
        {
					simplex<T,ScalarTypeT>* simp =  this->elements[i][j];
					grad.push_back( simp->gradationLevel );
					vector<int> vert = simp->sortedVertices();
					Filtration<ScalarTypeT>::simp2vert[i].push_back( vert );
					vert2simp[i][vert] = j;

             //delete this->elements[i][j]->vertCoords;
             //delete this->elements[i][j]->coBoundary;
        }
        gradationOfSimplicesInFollowingDimensions.push_back(grad);
    }

}

template <typename T, typename ScalarTypeT>
simplicialComplex<T,ScalarTypeT>::simplicialComplex( const std::vector< T >& pointCloud , double epsilonArg , int maxCreatedDimensionArg ):epsilon(epsilonArg),maxCreatedDimension(maxCreatedDimensionArg)
{
	createSimplicialComplex(pointCloud);
}

template <typename T, typename ScalarTypeT>
/**
 * @brief Implementation of Algorithm 4.5 DomFiltration 
 *
 * @param source
 * @param f
 */
simplicialComplex<T,ScalarTypeT>::simplicialComplex( simplicialComplex<T,ScalarTypeT> &source, Map<ScalarTypeT> f )
{

	std::map< simplex<T,ScalarTypeT>*,  simplex<T,ScalarTypeT>* > simp2simp;  
	for(int i=0;i<source.elements.size();i++)
	{
		std::vector< simplex<T,ScalarTypeT>* > simplices;
		int ind = 0;
		for(typename vector< simplex<T,ScalarTypeT>* >::iterator it=source.elements[i].begin();it!=source.elements[i].end();it++)
		{
			simplex<T,ScalarTypeT>* elem = new simplex<T,ScalarTypeT>;

			elem->nr = (*it)->nr;

			std::pair<int, int> img = f(ind,i);

			ind++;
			if(img.second == NOTFOUND) continue;
			double img_grad = source.simplexGradation(img.first, img.second);

			elem->gradationLevel = std::max( (*it)->gradationLevel, img_grad );
			for( typename std::vector< simplex<T,ScalarTypeT>* >::iterator it2=(*it)->boundary.begin();it2!=(*it)->boundary.end();it2++)
			{
				typename std::map< simplex<T,ScalarTypeT>*,  simplex<T,ScalarTypeT>* >::iterator b = simp2simp.find(*it2);
				if(b!=simp2simp.end())
					elem->boundary.push_back( b->second );
			}

			for( typename std::vector< simplex<T,ScalarTypeT>* >::iterator it2=(*it)->vertices.begin();it2!=(*it)->vertices.end();it2++)
			{
				typename std::map< simplex<T,ScalarTypeT>*,  simplex<T,ScalarTypeT>* >::iterator b = simp2simp.find(*it2);
				if(b!=simp2simp.end())
					elem->vertices.push_back( b->second );
			}

			simp2simp[*it]=elem;

			simplices.push_back(elem);
		}
		elements.push_back(simplices);
	}

	//at the end it would be good to sort simplices in elements according to gradation, so after we can easilly produce boundary matrices.
	for ( size_t i = 0 ; i != this->elements.size() ; ++i )
	{
		std::sort( this->elements[i].begin() , this->elements[i].end() , compare<T,ScalarTypeT> );
	}

	Filtration<ScalarTypeT>::simp2vert.resize(source.dimension()+1);
	vert2simp.resize(source.dimension()+1);

	for ( size_t i = 0 ; i != this->elements.size() ; ++i )
	{
		std::vector< double > grad;

		for ( size_t j = 0 ; j != this->elements[i].size() ; ++j )
		{
			simplex<T,ScalarTypeT>* simp =  this->elements[i][j];
			grad.push_back( simp->gradationLevel );
			vector<int> vert = simp->sortedVertices();
			Filtration<ScalarTypeT>::simp2vert[i].push_back( vert );
			vert2simp[i][vert] = j;
		}

		gradationOfSimplicesInFollowingDimensions.push_back(grad);
	}

}


}
#endif //_RIPS_HPP_
