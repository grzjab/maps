/** @file Reduction.hpp
 *  @brief Functions used in a matrix reduction 
 *
 *  @author Grzegorz Jablonski
 *  @year 2012
 *  @bug No known bugs.
 */
#if !defined(_REDUCTION_HPP_)
#define _REDUCTION_HPP_

#include <vector>
#include <algorithm>
#include <iostream>

#include "Interval.h"
#include "utils.hpp"
#include "filtration.hpp"

using namespace std;

namespace mappers{
	template<typename Scalar>
		/**
		 * @brief Multiply column col by a scalar a
		 *
		 * @param col
		 * @param a
		 */
		void multiply(vector<pair<int,Scalar> > &col, Scalar a)
		{
			for(typename vector<pair<int,Scalar> >::iterator it=col.begin();it!=col.end();it++)
				it->second *= a;
		}

	template<typename Scalar>
		/**
		 * @brief Addition of columns in sparse matrix representation
		 *
		 * @param col
		 * @param col2
		 * @param a
		 *
		 * @return col + a * col2
		 */
		vector<pair<int,Scalar> > add(const vector<pair<int,Scalar> > &col, const vector<pair<int,Scalar> > &col2, Scalar a)
		{
			vector<pair<int,Scalar> > res;
			typename vector<pair<int,Scalar> >::const_iterator it=col.begin(), it2=col2.begin(), itLast = col.end(), it2Last=col2.end();
			while(it!=itLast && it2!=it2Last)
			{
				if(it->first==it2->first)
				{
					Scalar coef = it->second + a* (it2->second);
					if(!isZero(coef))
					{
						res.push_back(make_pair(it->first,coef));
					}
					it++; it2++;
				}
				else if(it->first<it2->first)
				{
					res.push_back(*it);
					it++;
				}
				else
				{
					res.push_back(make_pair(it2->first,it2->second*a));
					it2++;
				}
			}
			while(it!=itLast)
			{
				res.push_back(*it);
				it++;
			}
			while(it2!=it2Last)
			{
				res.push_back(make_pair(it2->first,it2->second*a));
				it2++;
			}
			sort(res.begin(), res.end());
			return res;
		}

	template<typename Scalar>
		/**
		 * @brief Reduce boundary matrix in dimension dim using the algorithm from
		 * Chen, Chao and Michael Kerber. "Persistent homology computation with a twist." 2011.
		 *
		 * @param boundary
		 * @param diagram
		 * @param low
		 * @param filtration used for debug process only
		 * @param dim
		 */
		void reduceOneDimTwist(vector<vector<vector<pair<int,Scalar> > > > &boundary, 
				vector<vector<Interval> > &diagram, vector<vector<int> > &low, Filtration<Scalar> &filtration, int dim)
		{
			int n = boundary[dim].size();
			vector<pair<int, Scalar > > zeroColumn;
			vector<vector<pair<int,Scalar> > > D;

			while(boundary.size() <= dim+1)
			{
				boundary.push_back( vector<vector<pair<int,Scalar> > >() );
			}

			for(int i=0;i<n;i++)
			{
				vector<pair<int, Scalar > > col;
				col.push_back(std::make_pair<int, Scalar>(i, Scalar(1)));

				D.push_back(col);
			}

			//assert(n>0);
			PRINT("reduction, level:  "<<dim <<":number of simplices" << n <<"\n")
				//print(boundary); //,filtration,dim-1);

				for(int i=0;i<n;i++)
				{
					if(boundary[dim][i].empty()) //this means column was zeroed
						continue;
					//cerr<<dim<<":"<<i<<"\n";
					vector<pair<int,Scalar> > &column = boundary[dim][i];
					while(!column.empty() && low[dim-1][column.back().first]!=NONE)
					{
						//PRINT("adding to "<<i<<"column number "<<low[dim-1][column.back().first]<<"\n")
						//for(int k=0;k<column.size();k++)
						//	PRINT(column[k].first<<",")
						//PRINT("\n")
						//cerr<<"\n";
						//for(int k=0;k<boundary[dim][low[dim-1][column.back().first]].size();k++)
						//	PRINT(boundary[dim][low[dim-1][column.back().first]][k]<<",")
						//PRINT("\n")
						//print( boundary[low[column.back().first]] );
						//cerr<<"\n";
						Scalar coef = -column.back().second;
						int lastElementIndex = column.back().first;
						column = boundary[dim][i] = add(column, boundary[dim][low[dim-1][column.back().first]], -column.back().second);
						D[i] = add( D[i], D[low[dim-1][lastElementIndex]], coef);
					}

					//if(i%1000==0) cout<<i<<"\n";
					//interval that was just killed
					if(!column.empty())
					{
						double birth = filtration.simplexGradation(dim-1,column.back().first);

						boundary[dim-1][column.back().first] = zeroColumn;

						double death = filtration.simplexGradation(dim,i);

						low[dim-1][column.back().first] = i;

						if(column.back().second != Scalar(1))
						{
							Scalar coef = Scalar(1) / column.back().second;
							multiply(column, coef);
							multiply(D[i], coef);
						}
						//if(dim==2)
						//	PRINT("("<<birth<<";"<<death<<column.size()<<")\n")
						if(death==birth) continue;


						diagram[dim-1].push_back(Interval(birth,death,i));

						//cerr<<diagram.back();
						//print(column);
						//cerr<<"\n";
					}else{
						//if(dim==2)
						//	PRINT("NOT KILLED!")
						//new interval that will not be killed

						double birth = filtration.simplexGradation(dim,i);
						double death = 10000;

						//if(dim==1)
						//	cerr<<"Nie zabity! ("<<birth<<")\n";
						if ( D[i].back().second != Scalar(1) ){
							Scalar coef = Scalar(1) / D[i].back().second ;
							multiply( D[i], coef );
							multiply( boundary[dim][i], coef);
						}

						boundary[dim+1].push_back( D[i] );
						assert( low[dim][D[i].back().first] == NONE );
						low[dim][D[i].back().first] = boundary[dim+1].size()-1;
						diagram[dim].push_back(Interval(birth,death,boundary[dim+1].size()-1));
					}


				}
			if(dim==1) 
			{
				vector<pair<int,Scalar> > artificial;
				artificial.push_back(make_pair(0,Scalar(1)));
				boundary[dim].push_back(artificial);
				diagram[dim-1].push_back(Interval(0,INF,boundary[dim].size()-1));
				low[0][0]=boundary[dim].size()-1;
			}

		}


	template<typename Scalar>
		/**
		 * @brief Reduce boundary matrices up to a level maxDim
		 * Chen, Chao, and Michael Kerber. "Persistent homology computation with a twist."  2011.
		 *
		 * @param boundary
		 * @param diagrams
		 * @param low
		 * @param filtration used only for debugging
		 * @param maxDim
		 */
		void reduceTwist(vector<vector<vector<pair<int,Scalar> > > > &boundary, vector<vector<Interval> > &diagrams, 
				vector<vector<int> > &low, Filtration<Scalar> &filtration, int maxDim = 100)
		{
			int dim = filtration.dimension();
			dim = min(dim, maxDim);
			vector<vector<vector<pair<int,Scalar> > > > sboundary = filtration.sortedBoundary();
			diagrams.resize( dim + 1 );
			low.resize( sboundary.size() );
			for(int i=0;i<sboundary.size();i++)
				low[i].resize(sboundary[i].size(),NONE);

			for(int i=dim;i>=1;i--)
			{
				reduceOneDimTwist(sboundary,diagrams,low,filtration,i);
				//cerr<<"Reduced boundary in dim "<<i<<"\n";
				//print(sboundary[i],filtration,i-1);
				//cerr<<"\n";
			}
			boundary = sboundary;
			for(int i=0;i<diagrams.size();i++)
				sort(diagrams[i].begin(), diagrams[i].end());
		}


	template<typename Scalar>
		/**
		 * @brief Reduce matrix boundary in a level dim
		 *
		 * @param lowerBoundary used only to determine size of the low matrix
		 * @param boundary
		 * @param diagram
		 * @param low
		 * @param filtration used only for a debugging process
		 * @param dim
		 */
		void reduceOneDim(vector<vector<pair<int,Scalar> > > &lowerBoundary, vector<vector<pair<int,Scalar> > > &boundary, vector<Interval> &diagram, vector<int> &low, Filtration<Scalar> &filtration, int dim)
		{
			int n = boundary.size();
			low.resize(lowerBoundary.size(), NONE);

			assert(n>0);
			cerr<<"reduction "<<dim <<":number of simplices" << n <<"\n";
			//print(boundary); //,filtration,dim-1);

			for(int i=0;i<n;i++)
			{
				//cerr<<dim<<":"<<i<<"\n";
				vector<pair<int,Scalar> > &column = boundary[i];
				while(!column.empty() && low[column.back().first]!=NONE)
				{
					//cerr<<"adding to "<<i<<"column number "<<low[column.back().first]<<"\n";
					//print(column);
					//cerr<<"\n";
					//print( boundary[low[column.back().first]] );
					//cerr<<"\n";
					column = boundary[i] = add(column, boundary[low[column.back().first]], -column.back().second);
				}

				//if(i%1000==0) cout<<i<<"\n";
				//new interval
				if(!column.empty())
				{
					double birth = filtration.simplexGradation(dim-1,column.back().first);
					double death = filtration.simplexGradation(dim,i);

					low[column.back().first] = i;

					if(column.back().second != Scalar(1))
						multiply(column, Scalar(1)/column.back().second);

					if(death==birth) continue;

					diagram.push_back(Interval(birth,death,i));

					//cerr<<diagram.back();
					//print(column);
					//cerr<<"\n";

				}

			}
			if(dim==1) 
			{
				vector<pair<int,Scalar> > artificial;
				artificial.push_back(make_pair(0,Scalar(1)));
				boundary.push_back(artificial);
				diagram.push_back(Interval(0,INF,boundary.size()-1));
				low[0]=boundary.size()-1;
			}

		}

	template<typename Scalar>
		/**
		 * @brief Reduce boundary matrices in levels up to a maxDim
		 *
		 * @param boundary
		 * @param diagrams
		 * @param low
		 * @param filtration only for a debugging process
		 * @param maxDim
		 *
		 * @deprecated use reduceTwist
		 */
		void reduce(vector<vector<vector<pair<int,Scalar> > > > &boundary, vector<vector<Interval> > &diagrams, vector<vector<int> > &low, Filtration<Scalar> &filtration, int maxDim = 100)
		{
			int dim = filtration.dimension();
			dim = min(dim, maxDim);
			vector<vector<vector<pair<int,Scalar> > > > sboundary = filtration.sortedBoundary();
			for(int i=1;i<=dim;i++)
			{
				vector<Interval> diagram;
				vector<int> lowOneDim;
				reduceOneDim(sboundary[i-1],sboundary[i],diagram,lowOneDim,filtration,i);
				sort(diagram.begin(),diagram.end());
				//cerr<<"Reduced boundary in dim "<<i<<"\n";
				//print(sboundary[i],filtration,i-1);
				//cerr<<"\n";
				diagrams.push_back(diagram);
				low.push_back(lowOneDim);
				boundary.push_back(sboundary[i]);
			}
		}
}
#endif //_REDUCTION_HPP_
