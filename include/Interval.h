/** @file Interval.h
 *  @brief Declaration of a class representing persistence interval in a given dimension
 *  @author Grzegorz Jablonski
 *  @year 2014
 *  @bug No known bugs.
 */
#if !defined(_INTERVAL_H_)
#define _INTERVAL_H_

#include <iostream>

namespace mappers{
	/**
	 * @brief Class representing persistence interval in a given dimension
	 */
	struct Interval{
		/**
		 * @brief beginning of the persistence interval
		 */
		double birth;
		/**
		 * @brief end of the persistence interval
		 */
		double death;
		/**
		 * @brief Index of column in a reduced boundary storing the homology generator
		 */
		int index;
		Interval(double _birth,double _death, int _index=-1):birth(_birth),death(_death),index(_index) {}
	};

	std::ostream& operator<<(std::ostream &s, const Interval &i);

	bool operator<(const Interval &a, const Interval &b);
}

#endif //_INTERVAL_H_
