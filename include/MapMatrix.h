/** @file MapMatrix.h
 *  @brief Declarations of auxiliary procedures to reduce running time of computation of matrices \Phi_1,...\Phi_n and \Psi_1,...\Psi_n
 *
 *  @author Grzegorz Jablonski
 *  @year 2012
 *  @bug No known bugs.
 */
#if !defined(_MAPMAPTRIX_H_)
#define _MAPMAPTRIX_H_
#include <vector>

#include "Types.hpp"

/**
 * @brief Implementation of Algorithm 4.3 baseCoeff
 *
 * @param chain vector representing chain of simplices
 * @param reducedBoundary reduced boundary of a simplicial complex
 * @param low auxiliary vector describign reducedBoundary
 * @param col2gen auxiliary vector describing reducedBoundary
 *
 * @return linear combination of columns of reducedBoundary equal to the input argument chain
 */
ColumnType baseCoeff(const ColumnType &chain, SparseMatrixType  &reducedBoundary, 
		std::vector<int> &low, std::vector<int> &col2gen);
void prepareMapMatrixInOneDim(Map<ScalarType> &f, 
		const DiagramType &domDiagram, 
		const DiagramType &rngDiagram,
		SparseMatrixType  &domReducedBoundary,  
		SparseMatrixType &rngReducedBoundary, 
		std::vector<int> &rngLow,		
		int dim, 
		Filtration<ScalarType> &dom,
		Filtration<ScalarType> &rng,
		SparseMatrixType  &result);
void prepareMapMatrix(Map<ScalarType> &f, 
		const GradedDiagramType &domDiagrams, 
		const GradedDiagramType &rngDiagrams,
		const GradedSparseMatrixType  &domReducedBoundary,  
		const GradedSparseMatrixType  &rngReducedBoundary,
		vector<vector<int> > &rngLow,	
		int maxDim, 
		Filtration<ScalarType> &dom,
		Filtration<ScalarType> &rng,
		GradedSparseMatrixType &result);

#endif // _MAPMAPTRIX_H_
