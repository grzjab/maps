//GJ 2012
#if !defined(_FILTRATION_HPP_)
#define _FILTRATION_HPP_

#include <vector>
using namespace std;

template<typename Scalar>
class Filtration{
	public:
		vector<vector<vector<int> > > simp2vert;
		virtual vector<vector<vector<pair<int,Scalar> > > > sortedBoundary()=0;
		virtual double simplexGradation(int dim,int index)=0; 
		virtual int vertices2simplex(vector<int> vertices)=0;// TODO: &vertices
		virtual int dimension()=0;
};
#endif //_FILTRATION_HPP_
