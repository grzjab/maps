//GJ 2012
#if !defined(_MAP_HPP_)
#define _MAP_HPP_

#include <algorithm>
#include <map>
#include <set>
#include <vector>
#include <cassert>
#include <iostream>
#include <fstream>
#include <boost/foreach.hpp>

#include "filtration.hpp"
#include "Debug.h"
using namespace std;

//**********FUNCTION DECLARATIONS*******
int signature(std::vector<int>& a);
//**************************************

extern const int NOTFOUND;
extern const int NONE;
extern const double INF;

namespace mappers{
	template<typename Scalar>
		class Map{
			Filtration<Scalar> *domain;
			Filtration<Scalar> *range;
			vector<int> map;
			bool identity;

			std::vector<int> parseIntMap(std::istream &stream)
			{
				std::vector<int> map;
				for(string s; std::getline(stream, s); )
				{
					if(s.size() == 0 || s[0] == '#')
						continue;

					stringstream ss(s);

					for(int p;ss>>p; )
						map.push_back(p);
				}
				return map;
			}
			public :

			Map(vector<int> &_map):identity(false)
			{
				map = _map;
			}

			Map(string mapFileName):identity(false){
				ifstream mapFile(mapFileName.c_str());
				map = parseIntMap(mapFile);
			}

			Map():identity(true){
			}

			void setDomRng( Filtration<Scalar> *_domain, Filtration<Scalar> *_range )
			{
				domain = _domain;
				range = _range;
			}

			void setMap (std::vector<int> &_map)
			{
				identity = false;
				map = _map;
			}

			vector<pair<int,Scalar> > operator()(vector<pair<int,Scalar> > chain, int dim)
			{
				typename std::map<int, Scalar> result;
				//typename std::vector<std::pair<int,Scalar> > res2;
				//PRINT("operator()")
				//print(chain,*domain,dim);
				//PRINT("->")
				for(typename vector<pair<int,Scalar> >::iterator it=chain.begin(); it!=chain.end(); it++)
				{
					vector<int> vert = domain->simp2vert[dim][it->first];
					std::set<int> rngVert;
					for(int i=0;i<vert.size(); ++i)
					{
						if(!identity)
							vert[i] = map[vert[i]];
						rngVert.insert(vert[i]);
					}
					if(dim==rngVert.size()-1) 
					{
						Scalar coef = it->second * Scalar(signature(vert));

						sort(vert.begin(),vert.end());
						int rngSimp = range->vertices2simplex(vert);

						assert(rngSimp!=NONE);
						result[rngSimp] += coef;
						if(dim==0) //TODO: check
							result[rngSimp] = Scalar(1);

						//res2.push_back(make_pair(rngSimp,coef));
					}
				}
				//sort(result.begin(),result.end());
				//print(res2);
				//PRINT("\n");
				//	cerr<<"\n";
				vector<pair<int,Scalar> > resultV;
				for(typename std::map<int, Scalar>::iterator it=result.begin();it!=result.end();it++)
				{
					if(!isZero(it->second))
						resultV.push_back(make_pair(it->first,it->second));
				}
				return resultV;
			}

			pair<int,int> operator()(int simp,int dim)
			{
				vector<int> vert = domain->simp2vert[dim][simp];
				//copy(vert.begin(),vert.end(),output);
				//cerr<<"->";
				set<int> rngVert;
				for(int i=0;i<vert.size(); ++i)
				{
					int v = vert[i];
					if(!identity) v = map[v];
					rngVert.insert(v);
				}
				pair<int,int> result;
				result.first = rngVert.size()-1;
				//copy( rngVert.begin(), rngVert.end(), output);
				result.second = range->vertices2simplex(vector<int>( rngVert.begin(), rngVert.end() ));
				//cerr<< ">>"<<result.second<<"\n";
				return result;
			}
		};
}

#endif //_MAP_HPP_
