/** @file Options.h 
 *  @brief 
 *
 *  @author Grzegorz Jablonski
 *  @year 2014
 *  @bug No known bugs.
 */
#if !defined(_OPTIONS_H_)
#define _OPTIONS_H_
#include <iostream>
#include <fstream>
#include <string>

#include <boost/program_options.hpp>
#include <boost/program_options/config.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/detail/cmdline.hpp>
#include <boost/program_options/detail/parsers.hpp>

//passing arguments from command line
namespace po = boost::program_options;

using namespace std;

class Options {
	public:
		po::options_description desc; 
		bool ok;
		bool verbose;
		bool printGenerators;
		bool generalizedEigenvectors;
		bool saveOrdinaryPersistence;
		int maxDim;
		int minDim;
		int matrices;
		double maximumEpsilon;
		string pointsFile;
		string relationFile;
		string distancesFile;
		string lambdaFile;
		string outputFile;
		Options(int argc, char *argv[],std::string programType=""):
			desc("The aim of this software is to compute the persistent homology of a self map.\nAllowed options"), 
			ok(true), verbose(false), printGenerators(false),generalizedEigenvectors(false)
			{
				try{
					bool relation = false;
					if(programType.compare("relation")==0){
						relation = true;
					}

					desc.add_options()
						("help,h", "produce help message")
						("maxdim", po::value<int>()->default_value(1), "maximal dimension (note: program builds maxdim+1 - skeleton)")
						("lambda,l", po::value< string >()->required(), "file with candidates for eigenvalues")
						("verbose,v", "show additional information")
						("epsilon,e",po::value< double >()->default_value(1),"maximum treshold for Rips Simplicial complex, 1 = build full complex")
						("output,o",po::value< string >()->default_value("diagram.txt"),"file to store persistence diagram")
						("mindim",po::value<int>()->default_value(1),"minimum dimension to compute persistent homology")
						("generalized,g","generalized eigenspaces" )
						("matrices",po::value<int>()->default_value(100),"number of matrices in the tower")
						("ordinary","save ordinary persistence in file ord_diagram.txt")
						("generators","show eigenvectors");

					if(!relation){
						desc.add_options()
							("points,p", po::value< string >()->required(), "file with points");				
					}else{ 
						desc.add_options()
							("relation,r",po::value< string >()->required(),"file with relation description")
							("distances,d",po::value< string >()->required(),"file with distances");
					}

					po::variables_map vm;
					po::store(po::parse_command_line(argc, argv, desc), vm);

					if (vm.count("help")) {
						cout << desc << "\n";
						ok = false;
						return;
					}

					if(vm.count("verbose"))
					{
						verbose = true;
					}

					if(vm.count("generalized"))
					{
						generalizedEigenvectors=true;
					}

					if(vm.count("generators"))
					{
						printGenerators = true;
					}

					if(vm.count("ordinary"))
					{
						saveOrdinaryPersistence = true;
					}

					maxDim = vm["maxdim"].as<int>();
					maximumEpsilon = vm["epsilon"].as<double>();
					outputFile = vm["output"].as<string>();
					minDim = vm["mindim"].as<int>();
					matrices = vm["matrices"].as<int>();

					cout << "Maximum created dimension: " << maxDim << " \n";
					cout << "Minimum dimension: " << minDim << "\n";
					cout << "Maximum epsilon: " << maximumEpsilon << "\n";

					if(vm.count("lambda") ){
						lambdaFile = vm["lambda"].as<string>();
						cout << "File with lambdas: " << lambdaFile  << ".\n";
					}else{ 
						cout << " Lambda file name not present \n";
						ok = false;
					}

					if(vm.count("points") && !relation){
						pointsFile = vm["points"].as<string>();
						cout << "File with points: " << pointsFile << ".\n";
					}else if (vm.count("relation") && vm.count("distances") && relation){
						relationFile = vm["relation"].as<string>();
						cout << "File with relation: " << relationFile << ".\n";

						distancesFile = vm["distances"].as<string>();
						cout << "File with distances: " << distancesFile << ".\n";
					}
					else{
						ok = false;
						cout<<desc<<"\n";
					}

					po::notify(vm); 

				}catch(std::exception& e){
					std::cerr << "Error: " << e.what() << "\n";
				}
				catch(...){
					std::cerr << "Unknown error!" << "\n";
				}

			}
};
#endif //_OPTIONS_H_
