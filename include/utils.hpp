/** @file utils.hpp
 *  @brief Definitions and declarations of auxiliary template functions 
 *  @author Grzegorz Jablonski
 *  @year 2014
 *  @bug No known bugs.
 */
#if !defined(_UTILS_HPP_)
#define _UTILS_HPP_

#include <iostream>
#include <vector>
#include <sstream>
#include "capd/vectalg/Vector.hpp"
#include "LU.hpp"
#include "filtration.hpp"
#include <iterator>

template<typename type>
std::vector<type> readLambda(std::istream &stream)
{
	std::vector<type> lambda;

	for(string s; std::getline(stream, s); )
	{
		if(s.size() == 0 || s[0] == '#')
			continue;
		
		//std::cout << s << std::endl;

		stringstream ss(s);

		type v;
		ss >> v;

		lambda.push_back(v);
	}
	return lambda;
}

template<typename type>
std::ostream& operator<<(std::ostream& out,const std::vector<type> &vec)
{
	out<<"[";
	for(int i=0;i<vec.size();i++)
		out<<vec[i]<<((vec.size()==i+1)?"":",");
	out<<"]\n";
}

template<class matrix,class quotientSpace>
/**
 * @brief Implementation of Algorithm 4.2 restriction
 *
 * @param A matrix representation of a map
 * @param Q representation of the domain quotient space
 * @param R representation of the range quotinet space
 * @param B new matrix representation of a map
 */
void matrixRestriction(const matrix &A, const quotientSpace &Q,const quotientSpace &R, matrix &B)
{
	typedef typename matrix::ScalarType ScalarType;
	if(A.numberOfColumns()==0 || Q.first.numberOfColumns()==0 || R.first.numberOfColumns()==0) 
	{
		B.resize(0,0);
		return;
	}
	matrix im = A * Q.first;
	int k = R.first.numberOfColumns();
	B.resize(R.first.numberOfColumns(),Q.first.numberOfColumns());
	matrix Rext(R.first.numberOfRows(),R.first.numberOfColumns() + R.second.numberOfColumns());
	copy(R.first,Rext,0,0);
	copy(R.second,Rext,0,R.first.numberOfColumns());
	matrix LU;
	std::vector<int> pivot;
	LUdecomposition(Rext,LU,pivot);
	for(int i=0;i<im.numberOfColumns();++i)
	{
		capd::vectalg::Vector<ScalarType,0> x(Rext.numberOfColumns());
		//capd::vectalg::Vector<ScalarType,0> x2(Rext.numberOfColumns());

		//cout<<"Solve: "<<Rext<<"*x="<<im.column(i)<<endl;
		//if(!solveLinearEquation(Rext,im.column(i),x))
		//	throw "matrixRestriction::Could not solve linear equation!";
		LUsolve(LU,pivot,im.column(i),x);
		//if(x!=x2){
		//	std::cout<<"x="<<x<<" x2="<<x2<<" A="<<Rext<<" b="<<im.column(i)<<"LU="<<LU<<"\n";
		//	for(int j=1;j<=Rext.numberOfColumns();j++)
		//		cout<<pivot[j];
		//	throw "Error";
		//}
		for(int j = 0; j < k ; ++j)
			B[j][i]=x[j];
	}
}



///////////input/output///////////////
template<typename T1,typename T2>
ostream& operator<<(ostream &s, const pair<T1,T2> &p)
{
	s<<"("<<p.first<<";"<<p.second<<")";
	return s;
}

template<typename T1,typename T2>
void print(const vector<pair<T1,T2> > &v)
{
	for(int i=0;i<v.size();i++)
		cout<<v[i]<<" ";
}

template<typename T1>
void print(const vector<vector<pair<int,T1> > > &v)
{
	for(int i=0;i<v.size();i++)
	{
		for(int j=0;j<v[i].size();j++)
		{
			//copy(f.simp2vert[dim][v[i][j].first].begin(),f.simp2vert[dim][v[i][j].first].end(),output);
			cout<< v[i][j].first <<";";
		}
		cout<<"\n";
	}
}

template<typename T1>
void print(const vector<vector<pair<int,T1> > > &v,const Filtration<T1> &f,int dim)
{
	for(int i=0;i<v.size();i++)
	{
		for(int j=0;j<v[i].size();j++)
		{
			copy(f.simp2vert[dim][v[i][j].first].begin(),f.simp2vert[dim][v[i][j].first].end(),ostream_iterator<int>(cout, " "));
			//cerr<< v[i][j].first <<";";
			cout<<";";
		}
		cout<<"\n";
	}
}

template<typename T1>
void print(const vector<pair<int,T1> > &v,const Filtration<T1> &f,int dim)
{
	for(int j=0;j<v.size();j++)
	{
		copy(f.simp2vert[dim][v[j].first].begin(),f.simp2vert[dim][v[j].first].end(),ostream_iterator<int>(cout, " "));
		//cerr<< v[i][j].first <<";";
		cout<<";";
	}
}

template<typename T1,typename ostream_type>
void print(const vector<pair<int,T1> > &v,const Filtration<T1> &f,int dim,ostream_type &stream )
{
	for(int j=0;j<v.size();j++)
	{
		copy(f.simp2vert[dim][v[j].first].begin(),f.simp2vert[dim][v[j].first].end(),ostream_iterator<int>(stream, " "));
		//cerr<< v[i][j].first <<";";
		stream<<";";
	}
}


///////////////////////////////////////

#endif //_UTILS_HPP_
