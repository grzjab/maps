/** @file LU.hpp
 *  @brief This file contains implementation of the LU decomposition with pivoting 
 *  for a non-square! m by n matrix A
 *  If m > n, then L is unit lower trapezoidal and U is upper triangular. 
 *  If m < n, then U is upper trapezoidal, and L is unit lower triangular.
 *  Useful when one has to solve many problems of type Ax = b for diffrent b
 *  This problem appears in computation of the quotient M/N
 *
 *  This implementation is experimental and not tested!
 *
 *  @author Grzegorz Jablonski
 *  @year 2012
 *  @bug No known bugs.
 */
#ifndef _CAPD_MATRIXALGORITHMS_LU_HPP_
#define _CAPD_MATRIXALGORITHMS_LU_HPP_

#include <capd/matrixAlgorithms/intMatrixAlgorithms.hpp>
//#include <Eigen/Dense>
#include "Debug.h"

namespace capd{
	namespace matrixAlgorithms{

		template<class matrix, class sqMatrix, class intVector>
			bool LUdecomposition(const matrix& B,sqMatrix &LU,intVector& pivot){

				typedef typename matrix::ScalarType ScalarType;
				int m=B.numberOfRows();
				int n=B.numberOfColumns();
				pivot = intVector(std::max(m,n)+1);
				LU = B;
				int mn = std::min(m,n);
				int i,j,k,p;
				for(k = 1; k <=std::max(m,n)  ; k++)
					pivot[k] = k;
				for(k = 1; k <= mn; k++)
				{
					for( j =k; j<=m; ++j)
						if(!isZero(LU(j,k))){
							pivot[k]=j;
							break;
						}
					if(pivot[k] != k)
						rowExchange(LU,k,pivot[k]);
					if(isZero(LU(k,k))) return false;
					ScalarType mul = LU(k,k);
					for( i = k+1; i<=m; ++i)
						LU(i,k) /= mul;
					for( i = k+1; i<=m; ++i)
					{
						if(!isZero(LU(i,k)))
						{ 
							ScalarType mul = LU(i,k);
							for( j= k+1; j<=n; ++j)
								LU(i,j) -= mul * LU(k,j); 
						}
					}
				}

			}

		template<class matrix, class vector, class vector2, class pV>
			bool LUsolve(const matrix& LU,const pV &pivot,const vector & bconst, vector2& x)
			{
				typedef typename matrix::ScalarType ScalarType;
				int n = LU.numberOfColumns();
				int mn = std::min(n,LU.numberOfRows());
				x = vector2(n);
				vector2 b(bconst);
				for(int i=1;i<=mn;++i)
				{
					if(pivot[i]!=i) std::swap(b(i),b(pivot[i]));
					//ScalarType sum(b(i));
					x(i) = b(i);
					for(int j=1;j<i;++j)
					{
						x(i) -= LU(i,j) * x(j);
					}
					//if(isZero(L(i,i))) 
					//	return x(i) = 0;
					//x(i) = sum;// / L(i,i);
				}
				//std::cout<<x<<"\n";
				for(int i=n;i>=1;--i)
				{		
					if(pivot[i]!=i) std::swap(b(i),b(pivot[i]));
					if(isZero(LU(i,i)))
					{
						x(i) = ScalarType(0);
						continue;
					}
					//ScalarType sum(b(i));
					//x(i) = b(i);

					for(int j=i+1;j<=n;++j)
						x(i)-= LU(i,j) * x(j);

					x(i) /= LU(i,i);
				}
				return true;
			}

		/* ------------------------  ------------------------ */
		template<class matrix,class IntVector>
			void LUquotientBaseMatrix(
					const matrix& A_W,                       // input: basis of G \subset Z^p as columns of A_W,
					const matrix& A_V,                       // input: basis of H\subset G \subset Z^p as columns of A_V,
					matrix& A_U,                             // output: pseudobasis of G/H as columns of A_U,
					IntVector& A_orders                      // output: IntVector of finite orders
					// of elements of pseudobasis A_U
					){
				typedef typename matrix::ColumnVectorType VectorType;
				int p=A_W.numberOfRows();
				int m=A_W.numberOfColumns();
				int n=A_V.numberOfColumns();
				//	 std::cout<<"Size: "<<p<<" x "<<m<<", "<<A_V.numberOfRows()<<" x "<<n<<"\n";
				
				VectorType a2;
				matrix B(m,n);
				matrix LU(A_W);
				std::vector<int> pivot;
				LUdecomposition(A_W,LU,pivot);
				for(int j=0;j<n;++j){
					LUsolve(LU,pivot,A_V.column(j),a2);
					for(int i=0;i<m;++i) B[i][j]=a2[i];
				}
				matrix Q,Qinv,R,Rinv;
				int s,t;
				//PRINT("Computing quotient.. smithForm..");
				smithForm(B,Q,Qinv,R,Rinv,s,t);
				//PRINT("...done!\n");

				matrix WQ=A_W*Q;
				A_U=MatrixSlice<matrix>(WQ,1,p,s+1,m);
				A_orders=IntVector(n-s);    // finite orders greater than one
				for(int i=s;i<n;++i) A_orders[i-s]=B[i][i];
			}


		/* ------------------------  ------------------------ */
	} // end of namespace matrixAlgorithms

} // end of namespace capd;

#endif // _CAPD_MATRIXALGORITHMS_LU_HPP_

