//GJ 2012
#if !defined(_UTILS2_H_)
#define _UTILS2_H_

#include <vector>
#include <fstream>

#include "rips.hpp"
#include "Types.hpp"

std::vector<std::pair<rips::euclideanPoint, rips::euclideanPoint> > parseMap(std::istream &stream);
std::vector<rips::euclideanPoint> domainPoints(std::vector<std::pair<rips::euclideanPoint, int> > &map);
std::vector<std::pair<rips::euclideanPoint, int> > prepareMap(std::istream &stream);
std::vector<int> qProj(std::vector<std::pair<rips::euclideanPoint, int> > &map);
std::vector<double> prepareCriticalValues(GradedDiagramType &domDiagrams, GradedDiagramType &diagrams, int levels,int minDim, int maxDim);

#endif //_UTILS2_H_
