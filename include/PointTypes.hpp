/** @file PointTypes.hpp
 *  @brief Declarations of point types
 *  @author Grzegorz Jablonski
 *  @year 2014
 *  @bug No known bugs.
 */
#if !defined(_POINTTYPES_HPP_)
#define _POINTTYPES_HPP_
#include <vector>
#include <iostream>
#include <string>
#include <sstream>
#include <stdint.h>
#include <cmath>
#include <assert.h>
#include "Debug.h"

namespace rips{

	class euclideanPoint;

	double computeDistance( euclideanPoint &first , euclideanPoint &second );

	/**
	 * @brief Point in the Euclidean space
	 */
	class euclideanPoint
	{
		public:
			euclideanPoint( std::vector<double> elements )
			{
				for ( size_t i = 0 ; i != elements.size() ; ++i )
				{
					this->coords.push_back( elements[i] );
				}
			}
			euclideanPoint( euclideanPoint &a, euclideanPoint &b)
			{
				for ( size_t i = 0 ; i != a.coords.size() ; ++i )
				{
					this->coords.push_back( a.coords[i] );
				}
				for ( size_t i = 0 ; i != b.coords.size() ; ++i )
				{
					this->coords.push_back( b.coords[i] );
				}
			}
			std::vector<double> coords;
			friend std::ostream& operator << ( std::ostream& out , euclideanPoint point );

			friend bool operator == ( euclideanPoint& first , euclideanPoint& second )
			{
				if ( first.coords.size() != second.coords.size() ){return false;}
				for ( size_t i = 0 ; i != first.coords.size() ; ++i )
				{
					if ( first.coords[i] != second.coords[i] )
					{
						return false;
					}
				}
				return true;
			}
	};

}

#endif //_POINTTYPES_HPP_
