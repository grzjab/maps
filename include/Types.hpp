/** @file Types.hpp
 *  @brief Default types
 *
 *  @author Grzegorz Jablonski
 *  @year 2014
 *  @bug No known bugs.
 */
#if !defined(_TYPES_HPP_)
#define _TYPES_HPP_
#include <vector>
#include <map>
#include <cassert>

#include "capd/fields/rationalNumber.h"
#include "capd/rings/Zp.h"
#include "capd/vectalg/Matrix.hpp"
#include "capd/vectalg/Vector.hpp"

#include "Interval.h"

/**
 * @brief Field of rational numbers
 */
typedef rationalNumber<int> RationalIntType;
/**
 * @brief Default scalar type for all objects
 */
typedef Zp ScalarType;
/**
 * @brief Default matrix type
 */
typedef capd::vectalg::Matrix<ScalarType,0,0> MatrixType;
/**
 * @brief Default vector type (algebra)
 */
typedef capd::vectalg::Vector<ScalarType,0> VectorType;
/**
 * @brief Default vector of integers (algebra)
 */
typedef capd::vectalg::Vector<int,0> IntVectorType;
/**
 * @brief Quotient M/N where, M and N are linear spaces spanned by columns
 * M is the first element of a pair, N is the second
 */
typedef std::pair<MatrixType, MatrixType > QuotientSpaceType;

/**
 * @brief Entry in the sparse matrix: first element is the index of the value 
 * in a column
 */
typedef std::pair<int, ScalarType > EntryType;
/**
 * @brief Column in the sparse matrix, we store only non-zero elements
 */
typedef std::vector<EntryType> ColumnType;
/**
 * @brief Sparse matrix
 */
typedef std::vector<ColumnType> SparseMatrixType;
/**
 * @brief Object storing sparse matrix in every level.
 */
typedef std::vector<SparseMatrixType> GradedSparseMatrixType;


/**
 * @brief Tower of vector spaces (spanned by columns of the matrix) / linear maps
 */
typedef std::vector<MatrixType> TowerType;

/**
 * @brief Object storing sequence of matrices in diffrent levels
 */
typedef std::vector<TowerType> GradedTowerType;

typedef std::vector<QuotientSpaceType> SpaceTowerType;
typedef std::vector<SpaceTowerType> GradedSpaceTowerType;

/**
 * @brief Persistence diagram in one level
 */
typedef std::vector<mappers::Interval> DiagramType;

/**
 * @brief Persistence diagrams in all levels
 */
typedef std::vector<DiagramType> GradedDiagramType;

typedef std::map<ScalarType,SpaceTowerType> EigenvalueSpaceTowerType;

#endif //_TYPES_HPP_

