#/** @file PersistenceDgm.hpp
 *  @brief This file contains front-end for general persistence algorithm.
 *  Namely given a sequence of matrices A, class PersistenceDgm constructs 
 *  computes the persistence of basis vectors (called HomologyClass)
 *  @author Grzegorz Jablonski
 *  @year 2012
 *  @bug Size of vectors alfa and revAlfa is not calculated properly in a constructor PersistenceDgm(std::vector<MatrixT>& A)
 */include <vector>
#include <iostream>

#include "generalPersistenceAlgorithm.hpp"
#include "Debug.h"

template<class MatrixT,class SqMatrixT,class IntVectorT>
/**
 * @brief Stores in a user-friendly way information from
 * General Persistence Algorithm. 
 */
class PersistenceDgm
{
	public:
		/**
		 * @brief Change of basis matrices
		 */
		typename std::vector<SqMatrixT> V;

	private:
		typename std::vector<IntVectorT> alfa;
		typename std::vector<IntVectorT> revAlfa;

	public:
		struct HomologyClass
		{
			int birth;
			int death;
			int column;
			HomologyClass(int _birth, int _death, int _col):birth(_birth),death(_death),column(_col)
			{}
			friend bool operator<(const HomologyClass &a, const HomologyClass &b)
			{ return a.death - a.birth > b.death - b.birth; }
		};

		std::vector<HomologyClass> homologyClasses;
		PersistenceDgm()
		{}
		/**
		 * @brief 
		 *
		 * @param A sequence of linear maps from which persistence intervals are calculated
		 */
		PersistenceDgm(std::vector<MatrixT>& A)
		{
			int nc=0;
			//prepare alfa, revAlfa and V
			for(int k=0;k<A.size();k++){
				nc = A[k].numberOfColumns();
				if(k>0) nc = std::max(nc,A[k-1].numberOfRows());
				V.push_back(MatrixT::Identity(nc));
				alfa.push_back(IntVectorT(nc+10));
				revAlfa.push_back(IntVectorT(nc+10)); 
			}

			V.push_back(MatrixT::Identity(A.back().numberOfRows()));

			alfa.push_back(IntVectorT(nc+10));
			revAlfa.push_back(IntVectorT(nc+10)); 

			//run matching algorithm
			//for explanation see BASIS LEMMA, page 6 in Edelsbrunner, H., Jabłonski, G., & Mrozek, M. 
			//"The Persistent Homology of a Self-map."
			nmatching(0, alfa.size()-1, V, A, alfa, revAlfa);
			//PRINT("A: "<<A<<"\n")

			//calculate intervals from output alfa and revAlfa
			int r=0,s=alfa.size()-1;
			for(int t=r;t<=s;++t){
				for(int n=1;n<=V[t].numberOfColumns();++n){
					if((alfa[t][n]>0 && revAlfa[t][n] == 0) || (alfa[t][n]==0 && revAlfa[t][n]==0) ){
						int u=t; 
						int p=n;
						int b=u;
						int lastP;
						

						do{
							lastP = p;
							p=alfa[u][p]; 
							u++;
						}while(p!=0);

						//if(u>s) u--;
						homologyClasses.push_back(HomologyClass(b,u,lastP-1));
						//homologyClasses.push_back(HomologyClass(b,u,n-1));
						
					}
				}
			}

			sort (homologyClasses.begin(), homologyClasses.end());
		}
};

