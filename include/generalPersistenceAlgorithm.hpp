#if !defined(_MAPSPERSISTENCEALGORITHM_HPP_)
#define _MAPSPERSISTENCEALGORITHM_HPP_
#include <fstream>
#include "capd/vectalg/Vector.hpp"
#include "capd/vectalg/Matrix.hpp"
#include "capd/matrixAlgorithms/intMatrixAlgorithms.hpp"
#include <vector>
#include <iostream>

#include "Debug.h"

using namespace capd::matrixAlgorithms;
using namespace capd::vectalg;
using namespace std;

typedef capd::vectalg::Vector<int,0> IntVector;

/* ------------------------ General persistence algorithm  ------------------------ */

template<class matrix, class sqMatrix>
bool partColEchelon(int &m, int n,sqMatrix &B,matrix &N,matrix &M){
  int j;
  while(m<=M.numberOfRows()){
    j =n;
    while(j<=M.numberOfColumns() && M(m,j) == typename matrix::ScalarType(0)) ++j;
    if(j<=M.numberOfColumns()) break;
    ++m;
  }
  if(m>M.numberOfRows()) return true;
	if(n!=j){
	  columnExchange(M,B,n,j);
		rowExchange(N,n,j);
	}
  typename matrix::ScalarType lambda = typename matrix::ScalarType(1) / M(m,n);
	//following two lines ensures 1 on pivot position
	rowMultiply(N,n,M(m,n));
  columnMultiply(M,B,n,lambda);
  for(int j =n+1;j<=M.numberOfColumns();++j){
    lambda = - M(m,j)/M(m,n);
		if(lambda == typename matrix::ScalarType(0))
			continue;
    columnAdd(M,B,n,j,lambda);
		rowAdd(N,n,j,-lambda);
  }
  return false;
}
/* ------------------------  ------------------------ */

template<class matrix, class sqMatrix>
bool partMatching(int t, int &m, int n, std::vector<sqMatrix> &V, std::vector<matrix> &A, 
                std::vector<IntVector> &alfa, std::vector<IntVector> &revAlfa){
  if(n > A[t].numberOfColumns() ) return true;
	if(t>0){
 		if(partColEchelon(m,n,V[t],A[t-1],A[t])) return true;
	}
	else {
		matrix dummy(V[t].numberOfColumns(),1);
		if(partColEchelon(m,n,V[t],dummy,A[t]) ) return true;
	}
  alfa[t][n] = m;
  revAlfa[t+1][m] = n;
  for(int i =m+1;i<= A[t].numberOfRows(); ++i){
		if(A[t](i,n) == typename matrix::ScalarType(0))
			continue;
    typename matrix::ScalarType lambda = -A[t](i,n) / A[t](m,n);
    rowAdd(A[t], i, m, lambda);
    int p =i; int q=m; int u=t+1;
    while(p!=0){
      columnAdd(V[u],p,q,-lambda);
      p=alfa[u][p];
      q=alfa[u][q];
      ++u;
    }
  }
  return false;
}
/* ------------------------  ------------------------ */

template<class matrix, class sqMatrix>
void matching(int r, int s, std::vector<sqMatrix> &V, std::vector<matrix> &A, 
                std::vector<IntVector> &alfa, std::vector<IntVector> &revAlfa){
  for(int t = s-1; t>=r; --t){
    int m=0;
    int n=0;
    do{
      ++m;
      ++n;
      if(partMatching(t,m,n,V,A,alfa,revAlfa)) break;
    }while(true);

  }
}
/* ------------------------  ------------------------ */

template<class matrix, class sqMatrix>
bool weakMatching(int t, int m, int n, std::vector<sqMatrix> &V, std::vector<matrix> &A, 
                std::vector<IntVector> &alfa, std::vector<IntVector> &revAlfa){
  if(n > A[t].numberOfColumns() ) return true;
	if(t>0){
 		if(partColEchelon(m,n,V[t],A[t-1],A[t])) return true;
	}
	else {
		matrix dummy(V[t].numberOfColumns(),1);
		if(partColEchelon(m,n,V[t],dummy,A[t]) ) return true;
	}
  alfa[t][n] = m;
  revAlfa[t+1][m] = n;
  return false;
}
/* ------------------------  ------------------------ */
template<class matrix, class sqMatrix>
bool fullMatching(int t, int m, int n, std::vector<sqMatrix> &V, std::vector<matrix> &A)
{
  for(int i =m+1;i<= A[t].numberOfRows(); ++i){
		if(isZero(A[t](i,n)))
			continue;
    typename matrix::ScalarType lambda = -A[t](i,n) / A[t](m,n);
    rowAdd(A[t], i, m, lambda);
		assert(t+1<V.size());
		columnAdd(V[t+1], i, m, -lambda);
		if(t < A.size() -1 && A[t+1].numberOfColumns()>0){
			columnAdd(A[t+1], i, m, -lambda);
		}
  }
}
/* ------------------------  ------------------------ */
template<class matrix, class sqMatrix>
void nmatching(int r, int s, std::vector<sqMatrix> &V, std::vector<matrix> &A, 
                std::vector<IntVector> &alfa, std::vector<IntVector> &revAlfa){
  PRINT("Matching algorithm - first phase...\n");

  for(int t = s-1; t>=r; --t){
  	  SHOW( double( s-t) / double( s - r) << "%")
    int m=0;
    int n=0;
    do{
      ++m;
      ++n;
      if(weakMatching(t,m,n,V,A,alfa,revAlfa)) break;
    }while(true);
  }

  PRINT("Matching algorithm - second phase...\n");

	for(int t=r;t<=s-1;t++)
	{
		SHOW( double( t - r) / double( s - r) << "%")
		int m=0;
		int n=0;
		do{
			m++;
			n++;
			while( m<=A[t].numberOfRows() && isZero(A[t](m,n)) ) m++;
			if ( m > A[t].numberOfRows() ) break;
			fullMatching(t,m,n,V,A);
		}while( n<A[t].numberOfColumns() );
	}
	PRINT("Matching finished!\n");
}
/* ------------------------  ------------------------ */

#endif //end of _MAPSPERSISTENCEALGORITHM_HPP_
