import sys
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt

ver = 0.1

if( len(sys.argv)<2):
	print "Usage: python diagram.py input_file"
	exit(0)

plt.figure()
diagram_input = open(sys.argv[1])
info_line = False

colors = ['black', 'red']
types =['xb','.b','+b','ob']
label = ['1', '2', '-1', 'other']

dim =0;
maximum = 0
x = []
y = []
inf = [	]
lambdaType = 0
for l in diagram_input :
	if len(l) == 1 : 
		continue 
	if l[1] == '#' :
		dim = dim + 1
		info_line = True
		continue
	if info_line :
		print 'info_line'
		info_line = False
		continue;

	t = l.rsplit()
	if len(t)==1 :
		lam = int(t[0])
		if(lam==1):
			lambdaType = 0
		elif(lam==2):
			lambdaType = 1
		elif(lam==18):
			lambdaType = 2
		else :
			lambdaType = 3
		continue;

	b = float(t[0])
	maximum = max(maximum, b)
	d = float(t[1])
	if(b == d) :
		continue
	if(d> 1000) :
		inf.append( [b, lambdaType] )
		continue
	maximum = max(maximum, d)
	x.append(b)
	y.append(d)

	plt.plot(b,d,types[lambdaType],markerfacecolor='None')

maximum = 0.5#1.5 *  maximum

plt.axis([0, maximum, 0, maximum])
plt.grid(True)
plt.xlabel('birth')
plt.ylabel('death')
plt.title(sys.argv[2])
diag = [0, maximum]
plt.plot(diag,diag,"--k")
for lambdaType in (0,1,2,3) :
	b=d=10
	plt.plot(b,d,types[lambdaType],label=label[lambdaType],markerfacecolor='None')
plt.legend(numpoints=1)

plt.show()
#plt.savefig(sys.argv[2])
