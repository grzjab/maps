/** @file Towers.cpp
 *
 *  @author Grzegorz Jablonski
 *  @year 2014
 *  @bug No known bugs.
 */
#include <vector>
#include <boost/foreach.hpp>

#include "capd/matrixAlgorithms/intMatrixAlgorithms.hpp"

#include "LU.hpp"
#include "Types.hpp"
#include "Interval.h"
#include "Debug.h"

namespace mappers{
	void makeKsiTower(
			const std::vector<double> &epsilons, 
			const std::vector<std::vector<Interval> > &dom,
			int dim,
			GradedTowerType &epsilonSequence
			)
	{
		epsilonSequence.resize(dim+1);
		for(int i=0;i<=dim;i++)
		{
			std::vector<int> domCol, rngCol;
			for(typename std::vector<double>::const_iterator eps = epsilons.begin();eps!=epsilons.end();eps++)
			{
				domCol = rngCol;
				rngCol.clear();
				for(int j=0;j<dom[i].size();j++)
				{
					if(*eps>=dom[i][j].birth && *eps<dom[i][j].death)
						rngCol.push_back(j);
				}
				if( eps == epsilons.begin()) continue;

				int m = rngCol.size(),n=domCol.size();
				MatrixType mat(m,n);
				int ind = 0;
				for(int j=0;j<n;j++)
				{
					while(ind<m && rngCol[ind]<domCol[j])
						ind++;
					if(ind==m) break;
					if( rngCol[ind]==domCol[j] )
						mat[ind][j] = ScalarType(1);
				}

				epsilonSequence[i].push_back(mat);
			}
		}
	}

	void makeMapTower(const std::vector<double> &epsilons, 
			const GradedSparseMatrixType &mapMatrix,
			const GradedDiagramType &dom,
			const GradedDiagramType &rng,
			int dim,
			GradedTowerType &tower 
			)
	{
		tower.resize(dim+1);
		for(int i=0;i<=dim;i++)
			BOOST_FOREACH(double eps, epsilons)
			{
				std::vector<int> domCol, rngCol;
				for(int j=0;j<dom[i].size();j++)
				{
					if(eps>=dom[i][j].birth && eps<dom[i][j].death)
						domCol.push_back(j);
				}
				for(int j=0;j<rng[i].size();j++)
				{
					if(eps>=rng[i][j].birth && eps<rng[i][j].death)
						rngCol.push_back(j);
				}
				int m = rngCol.size(),n=domCol.size();

				MatrixType mat(m,n);
				for(int j=0;j<domCol.size();j++)
				{ 
					int ind=0;
					int col = domCol[j];

					if(i>0){
						for(int k=0;k<mapMatrix[i][col].size();k++)
						{
							while(ind<m && mapMatrix[i][col][k].first > rngCol[ind])
								ind++;
							if(ind==m) break;
							if(mapMatrix[i][col][k].first == rngCol[ind])
								mat[ind++][j]=mapMatrix[i][col][k].second;
							if(ind==m) break;
						}
					}else{
						for(int k=0;k<mapMatrix[i][col].size();k++)
						{
							int gen = mapMatrix[i][col][k].first;
							//PRINT("index: "<<ind)
							if( eps>=rng[i][gen].birth && eps< rng[i][gen].death)
							{
								while( gen != rngCol[ind])
									ind++;
								assert(ind<rngCol.size());
								mat[ind][j] = ScalarType(1);//mapMatrix[i][col][k].second;
								break;
							}
						}

					}
				}
				tower[i].push_back(mat);
			}
	}

	SpaceTowerType applyEigenfunctor(
			const TowerType   &phiSequence,
			const TowerType   &psiSequence, 
			ScalarType lambda					//input: eigenvalue
			) //output: kernel (p - lambda * q) / ( kernel p intersected with kernel (p - lambda * q) )
			{
				PRINT( "Computation of tower for eigenvalue " << lambda << "\n")
					SpaceTowerType eigenBase;
				//cout<<"eigenvalue: "<< lambda << "\n";
				for(int k=0;k<psiSequence.size();k++)
				{
					// SHOW ( psiSequence[k].numberOfRows() << "x"<< psiSequence[k].numberOfColumns());
					MatrixType kernel,
							   image,
							   m(psiSequence[k]-lambda*phiSequence[k]), 
							   mc,
							   kernelF,
							   quotKernel,
							   intersec,
							   fMatrix(phiSequence[k]);
					IntVectorType order;	
					mc = m;
					// find kernel of f
					capd::matrixAlgorithms::kernelImage(fMatrix,kernelF,image);
					// find kernel of f - lambda * inc
					capd::matrixAlgorithms::kernelImage(m,kernel,image);
					//PRINT("kernel:"<<kernel<<"\n");
					// compute intersection of kernel and kernelF
					capd::matrixAlgorithms::spaceIntersection(kernel,kernelF,intersec);
					// compute quotient group kernel / (kernel intersected kernelF)
					capd::matrixAlgorithms::quotientBaseMatrix(kernel,intersec,quotKernel,order);

					// quotKernel + intersect represents coset of computed quotient group
					// it is also our new base consisting of eigenvectors acting as representants of cosets
					eigenBase.push_back(QuotientSpaceType(quotKernel,intersec));

					SHOW( "     "<< k<<"/"<<psiSequence.size()<< ", size: "<<m.numberOfRows()<<"x"<<m.numberOfColumns() )
				}
				PRINT( "done!\n")
					return eigenBase;
			}

	SpaceTowerType applyGeneralizedEigenfunctor(
			const TowerType   &phiSequence,
			const TowerType   &psiSequence, 
			ScalarType lambda					//input: eigenvalue
			) //output: kernel (p - lambda * q) / ( kernel p intersected with kernel (p - lambda * q) )
			{
				SpaceTowerType eigenBase;
				for(int k=0;k<psiSequence.size();k++)
				{
					MatrixType kernel,
							   image,
							   m(psiSequence[k]-lambda*phiSequence[k]), 
							   mc,
							   kernelF,
							   quotKernel,
							   intersec,
							   fMatrix(phiSequence[k]);
					IntVectorType order;	
					mc = m;
					// find kernel of f
					capd::matrixAlgorithms::kernelImage(fMatrix,kernelF,image);
					// find kernel of f - lambda * inc
					capd::matrixAlgorithms::kernelImage(m,kernel,image);
					// compute intersection of kernel and kernelF
					capd::matrixAlgorithms::spaceIntersection(kernel,kernelF,intersec);
					// compute quotient group kernel / (kernel intersected kernelF)
					capd::matrixAlgorithms::quotientBaseMatrix(kernel,intersec,quotKernel,order);

					//generalized eigenvectors (lambda p - q)w2 = p w1, where w1 \in kernel
					//we glue whole kernel to matrix m and solve for new kernel (called kernel2)
					//we take only vectors representing matrix m
					MatrixType mExt(mc.numberOfRows(), mc.numberOfColumns() + quotKernel.numberOfColumns()), kernel2, quotKernel2, intersec2;
					capd::matrixAlgorithms::copy(mc, mExt, 0, 0);
					capd::matrixAlgorithms::copy(phiSequence[k] * quotKernel, mExt, 0, mc.numberOfColumns());
					capd::matrixAlgorithms::kernelImage(mExt, kernel2, image);

					kernel2 = MatrixSlice<MatrixType>(kernel2,1,mc.numberOfColumns(),1,kernel2.numberOfColumns());

					// compute intersection of kernel2 and kernelF
					capd::matrixAlgorithms::spaceIntersection(kernel2,kernelF,intersec2);

					// compute quotient group kernel2 / (kernel2 intersected kernelF)
					capd::matrixAlgorithms::quotientBaseMatrix(kernel2,intersec2,quotKernel2,order);


					eigenBase.push_back(QuotientSpaceType(quotKernel2,intersec2));


				}
				return eigenBase;
			}
}

