/** @file utils.cpp
 *  @brief Definitions of auxiliary functions 
 *  @author Grzegorz Jablonski
 *  @year 2014
 *  @bug No known bugs.
 */
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "Interval.h"
#include "Types.hpp"
#include <boost/foreach.hpp>

extern const double INF;

using namespace std;
using namespace mappers;
/**
 * @brief Computes signature of the permutation a. Complexity O(n^2)
 *
 * @param a permutation
 *
 * @return sign of the permuation a
 */
int signature(std::vector<int>& a)
{
	int n = a.size();
	int c = 0;
	for(int i=0;i<n; ++i)
	{
		for(int j=i+1;j<n; ++j)
		{
			if(a[i] > a[j])
			{
				++c;
			}
		}
	}
	return c % 2 ? -1 : 1;
}
namespace mappers {
	std::ostream& operator<<(std::ostream &s, const Interval &i)
	{
		s<<"["<<i.birth<<";"<<i.death<<")";
		return s;
	}

	bool operator<(const Interval &a, const Interval &b)
	{
		if(a.birth!=b.birth) return a.birth<b.birth;
		if(a.death!=b.death) return a.death<b.death;
		return false;
	}
}

std::vector<double> prepareCriticalValues(GradedDiagramType &domDiagrams, GradedDiagramType &diagrams, int levels,int minDim, int maxDim){
	vector<double> eps;
	//enforce value close to zero
	eps.push_back(0.000001);

	vector<double> criticalValues;
	for(int i=minDim;i<=maxDim;i++)
	{
		for(int j=0;j<domDiagrams[i].size();j++)
		{
			criticalValues.push_back(domDiagrams[i][j].birth);
			criticalValues.push_back(domDiagrams[i][j].death);
		}
		for(int j=0;j<diagrams[i].size();j++)
		{
			criticalValues.push_back(diagrams[i][j].birth);
			criticalValues.push_back(diagrams[i][j].death);
		}
	}
	sort(criticalValues.begin(),criticalValues.end());
	criticalValues.erase( std::unique( criticalValues.begin(), criticalValues.end() ), criticalValues.end() );
	for(int i=0;i<criticalValues.size();i++)
	{
		eps.push_back(criticalValues[i]*0.999999999);
		eps.push_back(criticalValues[i]);
		eps.push_back(criticalValues[i]*1.000000001);
	}
	sort(eps.begin(),eps.end());
	vector<double> eps_temp = eps;
	eps.clear();
	eps.push_back(eps_temp.front()); eps.push_back(eps_temp.back());
	for(int i=0;i<levels;i++)
		eps.push_back( eps_temp[i * eps_temp.size() / levels] );
	sort(eps.begin(),eps.end());
	eps.erase( std::unique( eps.begin(), eps.end() ), eps.end() );
	return eps;
}
