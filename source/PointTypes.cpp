/** @file PointTypes.cpp
 *  @brief Definitions of functions
 *  @author Grzegorz Jablonski
 *  @year 2014
 *  @bug No known bugs.
 */
#include <vector>

#include "PointTypes.hpp"
namespace rips{

	double computeDistance( euclideanPoint &first , euclideanPoint &second )
	{
		///TORUS
		   double x1 = first.coords[0];
		   double y1 = first.coords[1];

		   double x2 = second.coords[0];
		   double y2 = second.coords[1];

		   double dx = std::min( std::abs(x1-x2), 1 - std::abs(x1-x2));
		   double dy = std::min( std::abs(y1-y2), 1 - std::abs(y1-y2));

		   return sqrt(dx*dx + dy*dy);
		///TORUS

		if ( first.coords.size() != second.coords.size() )
		{
			return 0;
		}
		double suma = 0;
		//max 
		/*if(first.coords.size()==4)
		  {
		  double suma1=0,suma2=0;
		  for ( size_t i = 0 ; i != 2 ; ++i )
		  {
		  suma1 += (first.coords[i] - second.coords[i])*(first.coords[i] - second.coords[i]);
		  }
		  for ( size_t i = 2; i != 4 ; ++i )
		  {
		  suma2 += (first.coords[i] - second.coords[i])*(first.coords[i] - second.coords[i]);
		  }
		  if(suma1>suma2) suma = suma1;
		  else suma = suma2;

		  return sqrt(suma);
		  }*/
		//euclidean
		for ( size_t i = 0 ; i != first.coords.size() ; ++i )
		{
			suma += (first.coords[i] - second.coords[i])*(first.coords[i] - second.coords[i]);
		}
		return sqrt(suma);
	}//computeDistance
}
