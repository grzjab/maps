/** @file MapMatrix.cpp 
 *  @brief Definitions of auxiliary procedures to reduce running time of computation of matrices \Phi_1,...\Phi_n and \Psi_1,...\Psi_n
 *
 *  @author Grzegorz Jablonski
 *  @year 2012
 *  @bug No known bugs.
 */
#include "Types.hpp"
#include "map.hpp"
#include "Reduction.hpp"

using namespace mappers;

ColumnType baseCoeff(const ColumnType &chain, const SparseMatrixType  &reducedBoundary, std::vector<int> &low, std::vector<int> &col2gen)
{
	ColumnType result;
	ColumnType chainCopy(chain);
	while( !chainCopy.empty() )
	{
		int colLow = chainCopy.back().first;
		ScalarType coef = -chainCopy.back().second;
		//cerr<<"adding col "<<low[colLow]<<"\n";
		//print( reducedBoundary[low[colLow]]);
		assert(low[colLow]!=NONE);
		chainCopy = add(chainCopy, reducedBoundary[ low[colLow] ], coef );
		//cerr<<"wynik: ";
		//print(chainCopy);
		if( col2gen[ low[colLow] ] != NONE )
		{
			result.push_back(make_pair(col2gen[ low[colLow] ], coef));
		}
	}
	//sort(result.begin(),result.end());
	return result;
}

void prepareMapMatrixInOneDim(Map<ScalarType> &f, 
		const DiagramType &domDiagram, 
		const DiagramType &rngDiagram,
		const SparseMatrixType  &domReducedBoundary,  
		const SparseMatrixType &rngReducedBoundary, 
		std::vector<int> &rngLow,		
		int dim, 
		Filtration<ScalarType> &dom,
		Filtration<ScalarType> &rng,
		SparseMatrixType  &result)
{
	int n = domDiagram.size(), m = rngDiagram.size();
	vector<int> col2gen(rngReducedBoundary.size(),NONE);
	//cout<<"perpareMapMatrixInOneDim dim:" << dim<< "\n";
	for(int i=0;i<m;i++)
	{
		col2gen[rngDiagram[i].index] = i;
	}

	//cout<<"generatory w rng\n";
	//	for(int i=0;i<rngDiagram.size();i++)
	//	{
	//	print(rngReducedBoundary[rngDiagram[i].index], rng,dim);
	//	cout<<"\n";
	//	}


	//cerr<<"zredukowany brzeg rng:\n";
	//print(rngReducedBoundary);

	for(int i=0;i<domDiagram.size();i++)
	{
		ColumnType img = f( domReducedBoundary[domDiagram[i].index],dim );

		//cout<<"Generator ";
		//print(domReducedBoundary[domDiagram[i].index], dom,dim);
		//print(domReducedBoundary[domDiagram[i].index]);
		//cout<<" - obraz: ";
		//print(img,rng,dim);
		//print(img);
		//cout<<"\n baseCoeff";
		ColumnType c = baseCoeff(img, rngReducedBoundary, rngLow, col2gen);
		if( dim >0 )
			sort(c.begin(),c.end());
		else
			reverse(c.begin(), c.end());
		//print(c);
		//cout<<"\n";

		result.push_back( c );

	}
}

void prepareMapMatrix(Map<ScalarType> &f, 
		const GradedDiagramType &domDiagrams, 
		const GradedDiagramType &rngDiagrams,
		const GradedSparseMatrixType  &domReducedBoundary,  
		const GradedSparseMatrixType  &rngReducedBoundary,
		vector<vector<int> > &rngLow,	
		int maxDim, 
		Filtration<ScalarType> &dom,
		Filtration<ScalarType> &rng,
		GradedSparseMatrixType &result)
{

	for(int i=0;i<=maxDim;i++)
	{
		//cerr<<"Dim:"<<i<<"\n";
		SparseMatrixType mapMatrix;
		prepareMapMatrixInOneDim(f,domDiagrams[i],rngDiagrams[i],domReducedBoundary[i+1],
				rngReducedBoundary[i+1], rngLow[i], i, dom, rng, mapMatrix);
		result.push_back(mapMatrix);
	}
}

///END FILE
