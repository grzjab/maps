#include <iostream>
#include <string>
#include "rips.hpp"

using namespace std;

std::vector<std::pair<rips::euclideanPoint, rips::euclideanPoint> > parseMap(std::istream &stream){
	int domDim,rngDim; // domain and range dimensions
	stream >> domDim ;//>> rngDim; 
	rngDim = domDim;
	//std::cout<<"Dimensions: "<<domDim<<" "<<rngDim<<"\n";
	std::vector<std::pair<rips::euclideanPoint, rips::euclideanPoint> > points;	
	
	for(string s; std::getline(stream, s); )
	{
		if(s.size() == 0 || s[0] == '#')
			continue;

		stringstream ss(s);

		std::vector<double> p1;
		std::vector<double> p2;

    	double v;
		for(int i=0;i<domDim;i++)
		{
			ss >> v;
			p1.push_back(v);
			//std::cout<<v<<" ";
		}

		for(int i=0;i<rngDim;i++)
		{
			ss >> v;
			p2.push_back(v);
			//std::cout<<v<<" ";
		}
		//std::cout<<"\n";

		rips::euclideanPoint ep1(p1), ep2(p2);

		points.push_back(std::pair<rips::euclideanPoint, rips::euclideanPoint>(ep1, ep2));
	}

	return points;
}

int findNearesPoint(rips::euclideanPoint &p, std::vector<std::pair<rips::euclideanPoint, rips::euclideanPoint> > &map)
{
	int nearest = 0;
	double min_dist = rips::computeDistance(map[nearest].first,p);
	int i =0;
	for(std::vector<std::pair<rips::euclideanPoint, rips::euclideanPoint> >::iterator it=map.begin();it!=map.end();it++)
	{
		double dist = rips::computeDistance(it->first,p);
		if(dist < min_dist)
		{
			min_dist = dist;
			nearest = i;
		}
		i++;
	}
	return nearest;
}

std::vector<std::pair<rips::euclideanPoint, int> > prepareMap(std::vector<std::pair<rips::euclideanPoint, rips::euclideanPoint> > &map)
{
	std::vector<std::pair<rips::euclideanPoint, int> > r;
	for(std::vector<std::pair<rips::euclideanPoint, rips::euclideanPoint> >::iterator it=map.begin();it!=map.end();it++)
	{
		r.push_back(std::pair<rips::euclideanPoint, int>(it->first, findNearesPoint(it->second,map)));
	}
	return r;
}

std::vector<std::pair<rips::euclideanPoint, int> > prepareMap(std::istream &stream)
{
	std::vector<std::pair<rips::euclideanPoint, rips::euclideanPoint> > r=parseMap(stream);
	return prepareMap(r);
}


std::vector<rips::euclideanPoint> domainPoints(std::vector<std::pair<rips::euclideanPoint, int> > &map)
{
	std::vector<rips::euclideanPoint> r;
	for(std::vector<std::pair<rips::euclideanPoint, int> >::iterator it=map.begin();it!=map.end();it++)
	{
		r.push_back(it->first);
	}
	return r;
}

std::vector<int> qProj(std::vector<std::pair<rips::euclideanPoint, int> > &map)
{
	std::vector<int> r;
	for(int i=0;i<map.size();++i)
	{
		r.push_back(map[i].second);
	}
	//for(int i=0;i<r.size();++i)
	//	std::cout<<r[i]<<" ";
	return r;
}

