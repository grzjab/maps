# Persistent homology of self maps

Developers:
Grzegorz Jablonski

### Description

This project contains methods for computing the persistence of self maps as described in the paper [1]. The delaunay branch contains new implementation that uses Delaunay complexes and collapses from Cech complexes as described in [4].

All algorithms are implemented in C++ language. We use functions and
data structures from [Boost](http://www.boost.org/) and [CAPD](http://capd.ii.uj.edu.pl/) library.

### Installation

The software was compiled and tested on Ubuntu 14.04 operating system.
After unpacking the package execute in the main folder the command:
```
$ cmake .
&& make
```
After successful configuration and compilation there should be a binary file
named vrmp in the same folder.

### Usage

To compute λ-persistence diagrams execute:
```
$ ./vrmp -p file_with_points -l file_with_candidates
```
where file_with_points contains a set of points S ⊂ R n together with a map
g : S → S in the following format:
- the first line contains one integer number n
- the following lines contain 2n decimal numbers. In every line we define
a map g on a point p 1 ∈ R n by setting g(p 1 ) = p 2 . The first n numbers
are coordinates of a point p 1 , the following n numbers are coordinates of a
point p 2 .
The file file_with_candidates should contain candidates for eigenvalues in
separate lines.
To compute λ-generalized persistence diagrams execute:
```
$ ./vrmp -p file_with_points -l file_with_candidates -g
```

### Results

Results are stored in the file named diagram.txt. Persistence intervals are
listed first by dimension (0th, 1st dimension) and inside dimension by the
ordering of the candidates in the file file_with_candidates. Every persis-
tence interval is described in one line by two real numbers birth and death.
Dimensions are separated by a line containing #####. The persistence dia-
grams for eigenvalue 1 and 2 containing intervals [0; 2.10257], [0; 0.151285] for
eigenvalue 1 in dimension 1, and interval [0.0710019; 0.080787] for eigenvalue
2 in dimension 1 is shown below:
```
1
1
0 2.10257
0 0.151285
2
0.0710019 0.080787
#####
```
The first line denotes the dimension of homology.

### Plots

The python script diagram.py in the folder scripts is provided to generate
plots of diagrams from text files. To generate persistence diagram run:
```
$ ./diagram.py diagram.txt
```
The script diagram.py uses Matplotlib library.

### Examples

The input data used to generate plots are in the folder examples/[example_case].
Every file contains the σ parameter of the Gaussian noise in the name.

### Bibliography
[1] Edelsbrunner, Herbert, Grzegorz Jabłoński, and Marian Mrozek. "The persistent homology of a self-map." Foundations of Computational Mathematics 15.5 (2015): 1213-1244.

[2] Chen, Chao, and Michael Kerber. "Persistent homology computation with a twist." Proceedings 27th European Workshop on Computational Geometry. Vol. 11. 2011.

[3] Zomorodian, Afra. "Fast construction of the Vietoris-Rips complex." Computers & Graphics 34.3 (2010): 263-271.

[4] Bauer, Ulrich, and Herbert Edelsbrunner. "The Morse theory of\ v {C} ech and Delaunay complexes." arXiv preprint arXiv:1312.1231 (2013).

[5] Gärtner, Bernd. "Fast and robust smallest enclosing balls." Algorithms-ESA’99. Springer Berlin Heidelberg, 1999. 325-338.


**This project was supported by National Science Centre (Poland) DEC-2013/09/N/ST6/02995 Grant and by the TOPOSYS Project FP7-ICT-318493-STREP.**